/*
    This file is part of findRods2Dt.

    findRods2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    findRods2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with findRods2Dt.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <tiffio.h>
#include <stdlib.h>
#include <math.h>

void initialiaseHistogram(int* histogram, int* histogram_r, int* histogram_g, int* histogram_b, double* values, int BINS);
void process(int start, int stop, tImage_uni* images, tImage_uni* images_bg, int tophatdiameter, double blurdiameter, int bacteriaAreWhite, int RGB, char* outputdir, int DEBUG);
void tophat(tImage_uni* img, tImage_uni* newImage, float sigmax, float sigmay);
void tophatRGB(tImage_uni* img, tImage_uni* newImage, float sigmax, float sigmay);
void tophatBackground(tImage_uni* img, tImage_uni* newImage, float sigmax, float sigmay);
void blurIntense(tImage_uni* img, tImage_uni* newImg, float sigma);
void subBackground(double a, tImage_uni *img);
void blur(tImage_uni* img, tImage_uni* newImg, float sigma);

