/*
    This file is part of findRods2Dt.

    findRods2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    findRods2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with findRods2Dt.  If not, see <http://www.gnu.org/licenses/>.
*/

/* About: FindRods2Dt

findRods2Dt is designed to find E. coli bacteria in 2D phase-contrast 
microscopy images.
 
VERSION: master branch - this version has option to be wise with memory

If you use this program for a scientific publication:
	Please cite [T. Vissers et al., Science Advances, 4, 4, eaao1170, 2018]

Background:
	This code builds upon earlier software designed and written by Michiel Hermes 
	to find the positions and orientations of rod-shaped colloids, for which the 
	algorithm and its applications are described in detail elsewhere (Besseling, 
	T. H., et al. Journal of Physics: Condensed Matter 27.19 (2015): 194109). 
	Teun Vissers and Michiel Hermes altered and optimised existing code and 
	designed and added new parts to make it suitable for 2D data-sets of 
	microscopy images containing rod-shaped bacteria. 

*/

#include <tiffio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <glob.h>
#include "tiffio.h"
#include "filter.h"

#define SMALL_NUM  0.00000001

/* Variables: Global variables (indicated with CAPITAL LETTERS)
  DEBUG           - Sets the amount of checks and DEBUG output that is generated.
                    0 is minimal (no output), higher numbers will generate more output.
  SUBBACKGROUND   - Value of background noise that is subtracted from the filtered image.
  DIAMETER        - The average expected diameter of the rods.
  BLURDIAMETER    - The width of the Gaussian blur.
  MINVAL          - Minimum value of a local maximum to be considered as a particle.
  FILEMASK[1024]  - The files in which to search for rods.
  FILEMASKBG[1024] - The background images.
  POSITIONSFILE[1024]    - The coordinate are written to this file.
  OUTPUTDIR[1024] - The directory in which to put the output files.
  OVERLAPR        - The degree of allowed overlap. "1" is all overlap is filtered "0" no overlap filter.
                    An intermediate value of 0.8 means that particles are removed if they overlap by more than 0.2 of their diameter.
  WRITEFREQ       - Writes an output image every WRITEFREQ frame.
  TOPHATDIAMETER  - Diameter for the top-hat filter
  BGTHRESHOLD    - Threshold to filter out constant sources of background signal
  WHITE           - 1 if bacteria have high intensity (white), 0 for low intensity (black)
  RGB             - Set to 1 for rgb image, to 0 for monochrome.
  FINDRODS        - If set to 1 to find rods and output coordinates and orientations.
  TRACERR         - Variable used in traceBackbone.
  CHUNK		  - size of a chunk of images to evaluate
*/

float MINVAL=0;
float DIAMETER=5.0;
float BLURDIAMETER=2.0;
float SUBBACKGROUND=0.03;
char FILEMASK[1024]="";
char FILEMASKBG[1024]="";
char POSITIONSFILE[1024]="";
char OUTPUTDIR[1024]="";
int DEBUG=10;
int WHITE = 0;
float OVERLAPR = 0.00;
float BGTHRESHOLD = 0.4;
int TOPHATDIAMETER = 15;
int RGB = 0;
int WRITEFREQ = 200;
int FINDRODS = 1;
int TRACERR = 2;
int CHUNK = 100;

#include "namelist.h"

// input variables, which can be defined in findRods2Dt.in
NameList nameList[] = {
  NameR (MINVAL),
  NameR (DIAMETER),
  NameR (BLURDIAMETER),
  NameR (SUBBACKGROUND),
  NameC (FILEMASK),
  NameC (FILEMASKBG),
  NameC (POSITIONSFILE),
  NameC (OUTPUTDIR),
  NameI (DEBUG),
  NameI (WHITE),
  NameR (OVERLAPR),
  NameR (BGTHRESHOLD),
  NameI (TOPHATDIAMETER),
  NameI (RGB),
  NameI (WRITEFREQ),
  NameI (FINDRODS),
  NameI (TRACERR),
  NameI (CHUNK),
};

#include "namelist.c"

// 2D vector of int
typedef struct {
  int x;
  int y;
} tVectori;

tVectori veci(int x,int y){
  tVectori res;
  res.x=x;
  res.y=y;
  return res;
}

// 2D vector of float
typedef struct {
  double x;
  double y;
} tVector;

//maximum backbone length in pixels
#define MAXLENGTH 5000

typedef struct b_tmp {
  tVectori r[MAXLENGTH];
  float intensity[MAXLENGTH];
  float totint;
  int length;
  struct b_tmp *next;
  struct b_tmp *prev;
  int del;
  float rodLength;
  float rodWidth;
  float ar; //aspect ratio
  float ar2; //aspect ratio based on diameter input (width of the rod)
  float ar3; //aspect ratio based on diameter input (width of the rod)
  float accuracy;
  tVector rodPosition;
  tVector rodOrientation;
} tBackbone;

typedef struct {
  float MINVAL;
  tVector size;      /* The size of the box in pixels*/
  float diameter;  /* The diameter of the colloids in pixels*/
  tImage_uni *image;     /* The raw image data */
  tImage_uni *shapeimage;     /* The raw image data */
  int n_part;        /* The number of local maxima*/
  int nn;       /* The number of particles after filtering and refinement step */ //THB 03-09-2012
  tBackbone *bpart;    /* The found particles */
} tBox;

inline float max(const float a,const float b){
  return a>b?a:b;
}

inline float min(const float a,const float b){
  return a<b?a:b;
}

inline float dot3(const float *a,const float *b){
  return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

inline float dot(const tVector a,const tVector b){
  return a.x*b.x+a.y*b.y;
}

inline float sqr(const float x){
  return x*x;
}

/* Function: interpolate
   Interpolates an image.

   Parameters:
     img - The image.
     r - The position.

   Returns:
     The interpolated value of the image.

*/

float interpolate(tImage_uni *img, tVector r)
{

  // Note that each pixel is centred around a coordinate, so pixel (0,0) goes 
  // from -0.5 to 0.5 in both x and y direction. So effective the area covered 
  // by all pixels is from -0.5 to 1023.5.

  // For a standard bilinear interpolation algorithm we therefore need to 
  // use an offset of 0.5 in the x- and y-direction. 

  float *bitmap = img->bitmap;
  float width = img->width;
  float height = img->height;

  double res = 0;

  int xa = (int) floor(r.x-0.5+0.5); // note the 0.5 offset
  int ya = (int) floor(r.y-0.5+0.5);
  //int xa = (int) floor(r.x);
  //int ya = (int) floor(r.y);
  int xb=xa+1;
  int yb=ya+1;
  int w= width;
  int h= height;
  if(xa < 0) xa =0;
  if(ya < 0) ya =0;
  if(xb >= w) xb = w-1;
  if(yb >= h) yb = h-1;
  if(xb < 0) return 0.0;
  if(yb < 0) return 0.0;
  if(xa >= w) return 0.0;
  if(ya >= h) return 0.0;

  res += *(bitmap + xa +ya*w) * ( xb+0.5-(r.x+0.5) )*( yb+0.5-(r.y+0.5) ); // note the 0.5 offsets
  res += *(bitmap + xb +ya*w) * (-xb+0.5+(r.x+0.5) )*( yb+0.5-(r.y+0.5) );
  res += *(bitmap + xa +yb*w) * ( xb+0.5-(r.x+0.5) )*(-yb+0.5+(r.y+0.5) );
  res += *(bitmap + xb +yb*w) * (-xb+0.5+(r.x+0.5) )*(-yb+0.5+(r.y+0.5) );

  return res;
}

/* Function: pointLine
   Calculates the distance from a point (x,y) to a line segment from (0,0) to (lx,ly).

   Parameters:
     x - X coordinate of the point.
     y - Y coordinate of the point.
     lx - X coordinate of the end point of the line segment.
     ly - Y coordinate of the end point of the line segment.

   Returns:
     The distance between the line segment and the point.
*/
float pointLine(float x, float y, float lx, float ly)
{
  float dot=x*lx+y*ly;
  float res;
  float lengthsq=lx*lx+ly*ly;
  if(dot <= 0){
    res = x*x + y*y;
  } else if(dot >0 && dot < lengthsq) {
    res= sqr(x)+sqr(y) - dot*dot/(lengthsq);
  } else {
    res=sqr(lx-x)+sqr(ly-y);
  }
  return sqrt(res);
}

/* Function: distanceLineLine
   Calculates the distance between a lines segment from a1 to a2 and
   a line segment from b1 to b2 in 3D.

   Parameters:
     a1 - Pointer to x, y and z coordinate of the beginning of a.
     a2 - Pointer to x, y and z coordinate of the end point of a.
     b1 - Pointer to x, y and z coordinate of the beginning of b.
     b2 - Pointer to x, y and z coordinate of the end point of b.

   Returns:
     The distance between two line segments.
*/
float distanceLineLine(float *a1, float *a2, float *b1, float*b2)
{
  float u[3]={(a2[0]-a1[0]),(a2[1]-a1[1]),(a2[2]-a1[2])};
  float v[3]={(b2[0]-b1[0]),(b2[1]-b1[1]),(b2[2]-b1[2])};
  float w[3]={(a1[0]-b1[0]),(a1[1]-b1[1]),(a1[2]-b1[2])};
  float a = dot3(u,u);        // always >= 0
  float b = dot3(u,v);
  float c = dot3(v,v);        // always >= 0
  float d = dot3(u,w);
  float e = dot3(v,w);
  float D = a*c - b*b;       // always >= 0
  float sc, sN, sD = D;      // sc = sN / sD, default sD = D >= 0
  float tc, tN, tD = D;      // tc = tN / tD, default tD = D >= 0

  // compute the line parameters of the two closest points
  if (D < SMALL_NUM) { // the lines are almost parallel
    sN = 0.0;        // force using point P0 on segment S1
    sD = 1.0;        // to prevent possible division by 0.0 later
    tN = e;
    tD = c;
  }
  else {                // get the closest points on the infinite lines
    sN = (b*e - c*d);
    tN = (a*e - b*d);
    if (sN < 0.0) {       // sc < 0 => the s=0 edge is visible
      sN = 0.0;
      tN = e;
      tD = c;
    }
    else if (sN > sD) {  // sc > 1 => the s=1 edge is visible
      sN = sD;
      tN = e + b;
      tD = c;
    }
  }

  if (tN < 0.0) {           // tc < 0 => the t=0 edge is visible
    tN = 0.0;
    // recompute sc for this edge
    if (-d < 0.0)
      sN = 0.0;
    else if (-d > a)
      sN = sD;
    else {
      sN = -d;
      sD = a;
    }
  }
  else if (tN > tD) {      // tc > 1 => the t=1 edge is visible
    tN = tD;
    // recompute sc for this edge
    if ((-d + b) < 0.0)
      sN = 0;
    else if ((-d + b) > a)
      sN = sD;
    else {
      sN = (-d + b);
      sD = a;
    }
  }
  // finally do the division to get sc and tc
  sc = (fabs(sN) < SMALL_NUM ? 0.0 : sN / sD);
  tc = (fabs(tN) < SMALL_NUM ? 0.0 : tN / tD);

  // get the difference of the two closest points
  float dP[3] = {w[0]+(sc*u[0])-(tc*v[0]),w[1]+(sc*u[1])-(tc * v[1]),w[2]+(sc*u[2])-(tc*v[2])};

  return sqrt(dot3(dP,dP));   // return the closest distance
}

/* Function: setupBox
     Sets the box

   Parameters:
     box - The box struct.
     diameter - Diameter of the rods.
     imgwidth - Image with in pixels.
     imgheight - Image height in pixels.

*/
void setupBox(tBox *box, float diameter,float imgwidth, float imgheight){
  box->bpart=NULL;
  box->size.x=imgwidth;
  box->size.y=imgheight;
  box->diameter=diameter;
  box->n_part=0;
}

/* Function: addBackbone
     Adds a backbone to the linked list.

   Parameters:
     tBackbone - backbone of a rod
     box - the box struct
*/
void addBackbone(tBackbone* p, tBox *box)
{
  box->n_part++;
  tBackbone *q;
  if(box->bpart==NULL){
    p->next=NULL;
    p->prev=NULL;
    box->bpart=p;
  } else {
    q=box->bpart;
    while(q->next != NULL) q=q->next;
    p->next=NULL;
    p->prev=q;
    p->prev->next=p;
  }
}

/* Function: drawLineColorFast
     Draws a line on an image  

   Parameters:
     image - Pointer to image
     a - starting point of the line
     b - end point of the line
     F - maximum intensity of the line
*/
void drawLineColorFast(tImage_uni *image,tVector a,tVector b, float F)
{
  int x,y;
  int xbegin = image->width;
  int ybegin = image->height;
  int xend = 0;
  int yend = 0;

  if (a.x < xbegin) xbegin = a.x-2;
  if (b.x < xbegin) xbegin = b.x-2;

  if (a.x > xend) xend = a.x+2;
  if (b.x > xend) xend = b.x+2;

  if (a.y < ybegin) ybegin = a.y-2;
  if (b.y < ybegin) ybegin = b.y-2;

  if (a.y > yend)  yend = a.y+2;
  if (b.y > yend)  yend = b.y+2;

  if (xbegin < 0) xbegin = 0;

  if (ybegin < 0) ybegin = 0;

  if (xend > image->width) xend = image->width;

  if (yend > image->height) yend = image->height;

  for(x=xbegin;x<xend;x++) for(y=ybegin;y<yend;y++) {
    float r=pointLine(x-a.x,y-a.y,b.x-a.x,b.y-a.y);
    if(r<2.0) {
      image->bitmap[x+y*image->width]+=exp(-r*r*2.0)*F;
    }
  }
}

/* Function: findBackbones
     Finds all backbones in box->image.
     Each pixel larger than the box->MINVAL is considered as a potential backbone pixel.
     Loops over all pixels within a distance rx from each pixels.
     Pixels are part of the backbone if there are no brighter pixels within the
     search distance (a local maximum) or if there is only a single pixel within the
     search distance brighter than the pixel in question. If there are more bright
     pixels they must be on a streight line for the pixel to be considered as part of a backbone.

  Returns: 
     The routine returns a new image with all backbone pixels set to 1 in box->image and all other
     pixels set to 0.

   Parameters:
     box - the box struct
*/
void findBackbones(tBox *box)
{
  int i,j,x,y;
  int width=box->image->width;
  int height=box->image->height;
  int rx=2; //2 gives some small gaps // 3 gives lines 2 pixels wide
  if (DIAMETER > 4) rx =3;
  if (DIAMETER > 6) rx =4;

  float *newbitmap = (float*) malloc(sizeof(float)*width*height);
  float val, im, re, max, val2;
  int n;

  for(i=0;i<width;i++) for(j=0;j<height;j++) if(j<rx || j >=height-rx || i<rx || i >=width-rx){
    *(newbitmap + i + j*width)=0.0;
  } else {
    val=*(box->image->bitmap + i + j*width); // value of this pixel
    im=0.0;
    re=0.0;
    n=0;
    max=0.0;

    if (val > box->MINVAL) {
      for(y=-rx;y<=rx;y++) for(x=-rx;x<=rx;x++) if(x*x+y*y < rx*rx && (x !=0 || y != 0)){ //dirty
        val2=*(box->image->bitmap+i+x+(j+y)*width);
        if(val2 > max) max=val2;

        if(val2 >= val) {
          double a = atan2((double) x,(double) y);
          re += cos(2.0*a);
          im += sin(2.0*a);
          n++;
        }
      }
      if((n==0) || (val > max*0.5 && (re*re+im*im)/n > 0.95)) {
        *(newbitmap + i + j*width)=1.0; //MH FIX?
      } else {
        *(newbitmap + i + j*width)=0.0;
      }
    }  else   *(newbitmap + i + j*width)=0.0;
  }
  free(box->image->bitmap);
  box->image->bitmap=newbitmap;
}

/* Function: traceBackbone
     Groups all backbone pixels that are TRACERR or less pixels apart into a backbone.
     Loops over the neighbors withing TRACERR of the start pixel in bbimg.
     If any non zero pixels are found they are added to the list. The routine
     continues until all non zero neighbors of the pixels in the list have also been
     added to the list.

   Parameters:
     start - The inital pixel from where to strart grouping the neighbors.
     bbimg - The image containing the backbone pixels.
     b     - A bitmap that keeps track of the pixels that have been grouped.
     bb    - Returns the backbone pixels.
     bbl   - Returns the length of the backbone.
*/
void traceBackbone(tVectori start, tImage_uni *bbimg,int *b, tVectori *bb,int *bbl)
{
  int i, n=1;
  int width=bbimg->width;
  int height=bbimg->height;
  int rr=TRACERR; // hack
  int x,y,xx,yy,k;
  bb[0]=start;
  b[(start.y)*width+(start.x)] = n;
  do{
    k=0;
    int m=n;
    for(i=0;i<m;i++){ //loop over list

      xx=bb[i].x;
      yy=bb[i].y;
      for(y=-rr;y<=rr;y++) for(x=-rr;x<=rr;x++)  if(x*x+y*y<=rr*rr) {
        if(xx+x >=0 && xx+x <width &&yy+y >=0 && yy+y <height){
          if((b[(yy+y)*width+(xx+x)] == 0) && bbimg->bitmap[(yy+y)*width+(xx+x)] != 0.0) {
            bb[n].x=xx+x;
            bb[n].y=yy+y;
            k++;
            n++;
            if(n == MAXLENGTH) {printf("ERROR Maxlength exceeded\n");exit(-1);}
            b[(yy+y)*width+(xx+x)] = n;
          }
        }
      }
    }
  } while(k > 0);
  *bbl=n;
}

/* Function: connectBackbone
  This routine groups the backbone pixels into groups most likely belonging to a single rod.
  This routine loops over all pixels in box->image (the backbone image) which are not zero
  and groups them if into backbones by calling traceBackbone.
  (The intensity value is not used and set to 1.0)

  Returns:
    The found backbones are added to box->bpart. 

  Parameters:
      box - the box containing all the image in which to search for backbones.
*/
void connectBackbone(tBox *box)
{
  int width=box->image->width;
  int height=box->image->height;
  int *nb = (int*) malloc(sizeof(int)*width*height); //new bitmap
  int nbackbones=0;
  int bblength=0;
  tVectori bbcoords[MAXLENGTH];
  float *b=box->image->bitmap;
  if(box->image->width!=width || box->image->height != height) {printf("ERROR. Sizes that should match do not match.\n");exit(666);}

  int i,j,ii;
  tBackbone *p;
  for(i=0;i<width*height;i++) nb[i]=0;

  for(i=0;i<width;i++) for(j=0;j<height;j++) {
    if(*(b + i + j*width) != 0 && *(nb + i + j*width) == 0){
      nbackbones++;
      traceBackbone(veci(i,j), box->image,nb, bbcoords,&bblength);
      p=(tBackbone*) malloc(sizeof(tBackbone));
      p->length=bblength;
      p->totint=bblength;
      p->accuracy=1.0;
      for(ii=0;ii<bblength;ii++){
        p->r[ii].x=bbcoords[ii].x;
        p->r[ii].y=bbcoords[ii].y;
        p->intensity[ii]=1.0;
      }
      addBackbone(p,box);
    }
  }
  free(nb);
}

/* Function: fitPixels
     Fits a straight line to a set of pixels. 

   Returns:
     A pointer to 8 floats:

    - 0 x position of first end point
    - 1 y position of first end point
    - 2 length
    - 3 x component of normalized orientation vector.
    - 4 y component of normalized orientation vector.
    - 5 Center of mass x component
    - 6 Center of mass y component
    - 7 Residuals
*/

float* fitPixels(tVectori *p, float *val, int count)
{
  int j;
  tVector r, o, com;
  double mass,l,l1,l2;
  tVector *pr = (tVector*) malloc(sizeof(tVector)*count);

  com.x=0.0;
  com.y=0.0;
  mass=0.0;
  // calculate the centre of mass
  for(j=0;j<count;j++){
    com.x+=p[j].x*val[j];
    com.y+=p[j].y*val[j];
    mass+=val[j];
  }
  com.x/=mass; // centre of mass x
  com.y/=mass; // centre of mass y
  double sigmaxx=0;
  double sigmaxy=0;
  double sigmayy=0;

  // This calculates the ellements of the 2x2 covariance matrix
  // covariance matrix [ sigma^2_xx, sigma^2_xy; sigma^2_yx, sigma^2_yy]
  for(j=0;j<count;j++){
    pr[j].x=p[j].x-com.x; // x-distance to com
    pr[j].y=p[j].y-com.y; // y-distance to com
    sigmaxx += pr[j].x*pr[j].x * sqr(val[j]); //=sigma^2xx
    sigmaxy += pr[j].x*pr[j].y * sqr(val[j]); //=sigma^2xy=sigma^2yx
    sigmayy += pr[j].y*pr[j].y * sqr(val[j]); //=sigma^2yy
  }

  if(count==1) { // if there is only 1 pixel
    o.x=1.0;
    o.y=0.0;
    l=0.0;
  }
  if(sigmaxy==0)
  { // if the particle is pointing perfectly horizontal or vertical
    l1=1;
    l2=0;
    if(sigmayy>sigmaxx){ //particle is perfectly horizontal
      o.x=0.0;
      o.y=1.0;
    } else {  //particle is perfectly vertical
      o.x=1.0;
      o.y=0.0;
    }
  } else {
    double t=sigmaxx+sigmayy; //trace of the covariance matrix
    double d=sigmaxx*sigmayy-sigmaxy*sigmaxy; //determinant of the covariance matrix
    l1=t*0.5+sqrt(t*t*0.25-d); //Largest eigenvalue of the covariance matrix
    l2=t*0.5-sqrt(t*t*0.25-d); //Smaller eigenvalue
    double lv1=sqrt(sqr(l1-sigmayy)+sqr(sigmaxy)); //length eigenvector
    o.x=(l1-sigmayy)/lv1; //set to the orientation of the largest eigenvector
    o.y=sigmaxy/lv1;
  }

  //calc dot product to get projection along rod and find end points
  double maxd=0.0;
  double pp,mind=0.0;
  for(j=0;j<count;j++){
    pp= pr[j].x*o.x+pr[j].y*o.y;
    if(pp>maxd) maxd=pp;
    if(pp<mind) mind=pp;
  }
  r.x=com.x+mind*o.x;
  r.y=com.y+mind*o.y;
  l=maxd-mind;

  free(pr);
  float *res=(float*) malloc(sizeof(float)*8);
  res[0]= r.x;
  res[1]= r.y;
  res[2]= l;
  res[3]= o.x;
  res[4]= o.y;
  res[5]= com.x;
  res[6]= com.y;
  res[7]= sqrt(l1/l2);
  return res;
}

/* Function: smooth
     Convolution with a Gaussian kernel (Gaussian blur) to smooth 1d data

   Parameters:
     data - Float pointer to data to be smoothed. Data is overwritten with result.
     n - Length of the data.
     s - With of the Gaussian.
*/
void smooth(float *data, int n,float s)
{
  int i=0;
  int j=0;
  int r= 1+(int)(s*3.5);
  int ks= r*2+1;

  if(DEBUG >=20)
    printf("smooth: %i %f %i %i\n", n, s, r, ks);

  float *res=(float*) malloc(sizeof(float)*n);
  float *kernel=(float*) malloc(sizeof(float)*ks);
  float som;

  double temp = 1.0/(2.0*s*s);
  for(i=0;i<ks;i++) kernel[i]=exp(-(i-r)*(i-r)*temp);

  for(i=0;i<n;i++) 
  {
    res[i]=0.0;
    som=0.0;
    for(j=0;j<ks;j++) 
    {
      if(i+(j-r) >= 0 && (i+(j-r) < n)) 
      {
         res[i]+=data[i+(j-r)]*kernel[j];
         som+=kernel[j];
      }
    }
    res[i]/=som;
  }

  for(i=0;i<n;i++) 
	data[i]=res[i];

  free(res);
  free(kernel);
}

/* Function: grabPixels
     Returns a set of pixel values from the image that are closer than 0.5*diameter to the rod.

   Parameters:
     image - The image from which to take the pixel values.
     diameter - The diameter of the rod.
     pos - Position of the rod.
     orr - Orientation of the rod.
     rl - Length of the rod.
     r  - Pointer to the returned pixel positions.
     intt - Pointer to the returned pixel intensities.
     dist - Pointer to the distance to the rod for each pixel.
     count - pointer to the number of pixels returned. 
*/
void grabPixels(tImage_uni* image, float diameter, tVector pos, tVector orr, float rl, tVectori **r, float **intt, float **dist, int *count)
{
  int width=image->width;
  int height=image->height;
  float xa,xb,ya,yb;
  int x,y;

  int mx=(int) ceil(diameter/2.0-0.5)*2+1;
  int my=(int) ceil(diameter/2.0-0.5)*2+1;
  int xoff=(mx-1)/2+2;
  int yoff=(my-1)/2+2;
  float intensity;

  xa=pos.x-rl*orr.x*0.5;
  xb=pos.x+rl*orr.x*0.5;
  ya=pos.y-rl*orr.y*0.5;
  yb=pos.y+rl*orr.y*0.5;

  float d;
  *count=0;
  for(y=-yoff+min(ya,yb);y<=yoff+max(ya,yb);y++) for(x=-xoff+min(xa,xb);x<=xoff+max(xa,xb);x++){ //first loop to count only
    if(x>=0 && x<width && y>=0 && y<height ) {
      d=pointLine(x-xa,y-ya,xb-xa,yb-ya);
      if(d < diameter*0.5) {
        intensity = *(image->bitmap+x+(y)*width);
        if(intensity>0){
          (*count)++;
        }
      }
    }
  }

  if(*count > 0) {
    *r=(tVectori*) malloc(sizeof(tVectori)*(*count));
    *intt=(float*) malloc(sizeof(float)*(*count));
    *dist=(float*) malloc(sizeof(float)*(*count));
    int j=0;

    for(y=-yoff+min(ya,yb);y<=yoff+max(ya,yb);y++) for(x=-xoff+min(xa,xb);x<=xoff+max(xa,xb);x++){
      if(x>=0 && x<width && y>=0 && y<height ) {
        d=pointLine(x-xa,y-ya,xb-xa,yb-ya);
        if(d < diameter*0.5) {
          intensity = *(image->bitmap+x+(y)*width);
          if(intensity>0){
            (*intt)[j]=intensity;
            (*dist)[j]=d;
            (*r)[j].x = x;
            (*r)[j].y = y;
            j++;
          }
        }
      }
    }

  }
}


/* Function:fitHist
     Used to obtains the ends of a rod from a histogram of intensity along the length of the rod.
     Calculates the average and maximum value of the histogram. 
     Returns the points where the intensity crosses through half this value at each side of the maximum value.
    
   Parameters:
     hist - The histogram to fit
     nhist - The number of elements in the histogram.
     aa - Pointer to the found begin point.
     bb - Pointer to the found end point.

*/

void fitHist(float* hist,int nhist,float *aa,float *bb, int hi)
{
  int j;
  int m=(nhist)/2; //middle

  if(nhist<=2) {
    *aa=0;
    *bb=0;
    return;
  }

  float avv=0.0; //average value
  float maxv=0.0; //maximum value
  int navv=0; //number of averages
  for(j=0;j<nhist;j++){
    if(hist[j] > maxv) maxv=hist[j];
  }

  for(j=0;j<nhist;j++) if(hist[j] > 0.15*maxv) {
    navv++;
    avv+=hist[j];
  }
  avv/=navv;

  if(DEBUG >=10 && hi > 0)
  { 
        // USED FOR debugging
  	FILE *fp_test;
	//print the histogram
	char filenamebuffer[1024];
	snprintf(filenamebuffer, sizeof(filenamebuffer), "%slengthfit_%.05i.dat", OUTPUTDIR, hi);
	fp_test = fopen(filenamebuffer,"w");
	for(j=0;j<nhist;j++) 
	{
		//printf("%i %f \n",j-m,hist[j]);
		fprintf(fp_test, "%i %f \n",j-m,hist[j]);
	}
	fclose(fp_test);
  }
  //find the beginning and the end of the rod
  double endval=0.5*avv;

  int begin=m,end=m;

  for(j=m;j<nhist;j++) {
    if(hist[j] < endval) {end=j;j=nhist;}
  }

  for(j=m;j>=0;j--) {
    if(hist[j] < endval) {begin=j;j=-1;}
  }

  if(begin<0) begin=0;

  if(end>nhist-2) end=nhist-2;

  // begin is the first bin towards one end where its value is lower than endval. The real end is a bit forward
  *aa= begin+(endval-hist[begin])/(hist[begin+1]-hist[begin]);

  //printf("a: %f\n", (endval-hist[begin]) );
  //printf("a: %f\n", (hist[begin+1]-hist[begin]) );
  // end is the first bin towards the other end where its value is lower than endval. The real end is a bit back
  //*bb= (end-1)+(endval-hist[end-1])/(hist[end]-hist[end-1]);
  *bb= (end-1)+(endval-hist[end-1])/(hist[end]-hist[end-1]);
  //printf("b: %f\n", (endval-hist[end-1]) );
  //printf("b: %f\n", (hist[end]-hist[end-1]) );

  if(DEBUG >=10 && hi > 0)
  {
        // USED FOR debugging
        FILE *fp_test;
        //print the histogram
        char filenamebuffer[1024];
        snprintf(filenamebuffer, sizeof(filenamebuffer), "%sselectedpart_%.05i.dat", OUTPUTDIR, hi);
        fp_test = fopen(filenamebuffer,"w");
        for(j=0;j<nhist;j++)
        {
		if (j > *aa && j < *bb)
		{
                	//printf("%i %f \n",j-m,hist[j]);
                	fprintf(fp_test, "%i %f \n",j-m,hist[j]);
		}
        }
        fclose(fp_test);
  }

  if(*aa<0)*aa=0;
  if(*bb>nhist) *bb=nhist;
  if(*aa>m)  *aa=m;
  if(*bb<m)  *bb=m;

  if(DEBUG >= 10) printf("%f %f (%f) %f (%f): %f %f %i %i\n",endval, hist[begin], hist[begin+1], hist[end], hist[end-1], *aa,*bb, begin, end);
}

/* Function: fitOrientation
     Calculates the position, orientation and approximate length of a rod.
     
     Parameters:
     	box - the box struct
	pos - approximate position of the rod
	orr - approximate orientation of the rod
	l - approximate length of the rod
	diameter - estimated diameter of the rod
*/
float* fitOrientation(tBox *box,tVector pos,tVector orr, float l, float diameter) 
{
  tVectori *p;
  float *val;
  float *dist;
  int count;
  grabPixels(box->image,box->diameter, pos, orr, l, &p, &val, &dist, &count);
  if(count==0) return NULL;

  float *fit=fitPixels(p,val,count);
  free(p);
  free(val);
  free(dist);
  return fit;
}

/* Function: fitLength
      Fits the length of a rod by calculating the intensity along the length of the rod.

    Parameters:
      box - the box structure 
      pos - approximate position of the rod
      orr - approximate orientation of the rod
      rl - approximate length of the rod
      fit - fit parameters
      diameter - estimated diameter of the rod

    Returns:
      Array containing new POLE.x, POLE.y en length of the rod
*/
float *fitLength(tBox *box,tVector pos,tVector orr,float rl,float *fit, float diameter, int hi)
{
  //construct histogram
  int i;
  double v,binsize=0.2;
  float grow=0.5; //Calculates histogram along a line that is longer by grow*diameter
  tVector r;
  //printf("adad: %f %f\n", rl, pos.x - orr.x*grow*rl - orr.x*rl*0.5);
  int nhist=(int) ((rl*(1.0+2.0*grow))/binsize);

  float *hist=(float*) malloc(sizeof(float)*nhist);
  for(i=0;i<nhist;i++)
  {
    r.x=pos.x+orr.x*binsize*i-orr.x*grow*rl-orr.x*rl*0.5; // centre of mass x - half axis in x-direction - extra part (grow) in x-direction + offset (i = 0 means pole)
    r.y=pos.y+orr.y*binsize*i-orr.y*grow*rl-orr.y*rl*0.5; // centre of mass y - half axis in y-direction - extra part (grow) in y-direction + offset (i = 0 means pole)
    v=interpolate(box->image,r);
    hist[i]=v;
    //printf("%i %i\n", i, nhist);
  }
  //smooth(hist,nhist, 0.7);
  smooth(hist,nhist, 0.7); // some smoothening
  float aa,bb;
  if (DEBUG >=20)
	printf("Length fit starts here (%i)\n", nhist);  

  fitHist(hist,nhist,&aa,&bb, hi);

  if(bb-aa <0) return NULL;

  free(hist);

  float *res=(float*) malloc(sizeof(float)*3);
  //printf("%f %f\n", orr.x, orr.y);
  //printf("bdbd: %f %f\n", rl, pos.x - orr.x*grow*rl - orr.x*rl*0.5);
  res[0] = pos.x + orr.x*binsize*aa - orr.x*grow*rl - orr.x*rl*0.5; // new x POLE coordinate from old midpoint - half old length - grow part + coordinate along fit axis
  res[1] = pos.y + orr.y*binsize*aa - orr.y*grow*rl - orr.y*rl*0.5;
  res[2] = (bb-aa)*binsize; // length calculated here
  return res;      
}

/* Function: fitWidth
      Fits the length of a rod by calculating the intensity along the length of the rod.

    Parameters:
      box - the box structure 
      pos - approximate position of the rod
      orr - approximate orientation of the rod
      l - approximate length of the rod
      fit - fit parameters
      diameter - estimated diameter of the rod

    Returns:
      The width of the rod.
*/
float fitWidth(tBox *box,tVector pos,tVector orr,float l,float* fit,float diameter, int hi)  
{
  tVectori *p;
  float *val;
  float *dist;
  int count;
  //Take more pixels to make sure the width can be fit accurately.
  grabPixels(box->image,box->diameter*1.5, pos, orr, l , &p, &val , &dist, &count);
  if(count==0) return -1;

  int j,pp,pw;
  tVector *pr = (tVector*) malloc(sizeof(tVector)*count);
  //Subtract the center
  for(j=0;j<count;j++){
    pr[j].x=p[j].x - (fit[0]+fit[3]*fit[2]*0.5);
    pr[j].y=p[j].y - (fit[1]+fit[4]*fit[2]*0.5);
  }

  int nmax=0;
  for(j=0;j<count;j++){
    pp= fabs(pr[j].x*fit[3] + pr[j].y*fit[4]);
    if((int) pp+1 > nmax) nmax=(int) pp+1;
  }
  nmax+=3;
  float binsize=0.04;
  nmax*=(1.0/binsize);
  int nhist=nmax*2+1;
  float *histw=(float*) malloc(sizeof(float)*nhist);
  float *histwi=(float*) malloc(sizeof(float)*nhist);
  for(j=0;j<nhist;j++){histw[j]=0.0;histwi[j]=0.0;}
  histw+=nmax;
  histwi+=nmax;

  //Construct the histogram
  for(j=0;j<count;j++){
    pw= (int) ((pr[j].x*fit[4] - pr[j].y*fit[3])/binsize);
    if(pw>=-nmax && pw<=nmax){
      histw[pw]+=val[j];
      histwi[pw]+=1.0;
    }
  }

  //Smooth the histogram
  smooth(histw-nmax,nhist,0.20/binsize);
  smooth(histwi-nmax,nhist,0.20/binsize);

  //Normalize the histogram
  for(j=-nmax;j<=nmax;j++){
    if(histwi[j] > 0.0) histw[j]/=histwi[j]; else histw[j]=0.0;
  }

  if(DEBUG>2) printf("fithist..\n");

  float aw,bw;
  if (DEBUG >=20)
	printf("Width fit starts here (%i) \n", nhist);
  fitHist(histw-nmax,nhist,&aw,&bw, -100);
  aw-=nmax;
  bw-=nmax;

  aw*=binsize;  bw*=binsize;

  free(histw-nmax);
  free(histwi-nmax);
  free(pr);

  free(p);
  free(val);
  free(dist);

  return (bw-aw);            // width of the bacteria
}

/* Function: fitBackbones
     Fits the rough orientation, position and length of all backbones.

   Parameters:
     box - the box struct
*/

void fitBackbones(tBox *box)
{
  tVectori r[MAXLENGTH];
  float val[MAXLENGTH];

  tBackbone *p;
  for(p=box->bpart;p!=NULL;p=p->next) {
    int j;
    for(j=0;j<p->length;j++){
      r[j].x=p->r[j].x;
      r[j].y=p->r[j].y;
      val[j]=p->intensity[j];
    }
    float *fit;
    fit=fitPixels(r,val,p->length);
    p->rodPosition.x=fit[0]+fit[2]*fit[3]*0.5;
    p->rodPosition.y=fit[1]+fit[2]*fit[4]*0.5;
    //printf("1. %f %f\n", p->rodPosition.x, p->rodPosition.y); 
    p->rodLength=fit[2];
    p->rodOrientation.x=fit[3];
    p->rodOrientation.y=fit[4];
    p->del=0;
    free(fit);
  }
}

/* Function: fitBackboneRefine
     Refines the backbone fits.

   Parameters:
     box - the box struct
*/

void fitBackboneRefine(tBox *box)
{
  if(DEBUG >= 5) printf("Start refining...\n");
  tBackbone *p=box->bpart;

  float CAL1 = 0.07;
  float CAL2 = 0.25;

  int i;
  for(i=0;p!=NULL;i++) {
    if(DEBUG > 100) printf("Fitting particle %i \n", i);

    // insert funny exception
    // Grab some extra pixels to make sure the whole rod is in there.
    // To do this, we elongate the rod a bit with factor CAL2
    
    float *fit=fitOrientation(box,p->rodPosition,p->rodOrientation,(1+2*CAL1)*p->rodLength,box->diameter);
    //float *fit=fitOrientation(box,p->rodPosition,p->rodOrientation,p->rodLength,box->diameter);
  
    if (fit != NULL) {
      p->rodOrientation.x=fit[3]; // orientation x
      p->rodOrientation.y=fit[4]; // orientation y
      p->rodPosition.x=fit[0]+fit[2]*fit[3]*0.5; // x-coordinate of com
      p->rodPosition.y=fit[1]+fit[2]*fit[4]*0.5; // y-coordinate of com
      //printf("2. %f %f\n", p->rodPosition.x, p->rodPosition.y);
      p->accuracy=fit[7]; //accuracy of orientation fit (squared risiduals?)

      // we elongate the rod a bit with factor CAL2
      float l = fit[2]*(1.0+CAL2); // elongate rod
      //float l = fit[2];

      p->rodWidth=fitWidth(box,p->rodPosition,p->rodOrientation, l, fit,box->diameter, i);  // this is the width 
      float *res = fitLength(box, p->rodPosition,p->rodOrientation, l, fit, box->diameter, i);
      if(res != NULL)
      {
	//printf("..\n");
        p->rodPosition.x=res[0]+res[2]*fit[3]*0.5; // calculate new centre of mass x from one of the poles
        p->rodPosition.y=res[1]+res[2]*fit[4]*0.5; // calculate new centre of mass y from one of the poles
        p->rodLength=res[2]; //rod length;
        free(res);    
      } else {
        p->del=1;
      }

      // attributing final properties on basis of fits:
      p->ar=p->rodLength/p->rodWidth;        // raw aspect ratio, taking the width from the previous fit
      p->ar2=(p->rodLength-p->rodWidth)/p->rodWidth;  // width, taking the width from the previous fit
      p->ar3=p->rodLength/DIAMETER;       // aspect ratio sphere-to-sphere in spherocylinder / diameter given in input file

      // freeing up memory
      free(fit);
    } else {
      p->del=1;
    }

    p=p->next;
  }
}


/* Function: drawRods
   Draws crosses on the image at the positions of the rods stored in box->bpart on image. 
   Draws two lines one along the length of the rod and one perpendicular to indicate the width. 

   Parameters:
     box - the box struct
     image - the image to draw on
*/
void drawRods(tBox *box,tImage_uni *image)
{
  tVector begin,end;
  tVector com, rodperp, pb, pe;

  tBackbone *p;
  for(p=box->bpart;p!=NULL;p=p->next) if(!p->del) {
    begin.x=p->rodPosition.x-p->rodLength*p->rodOrientation.x*0.5;
    begin.y=p->rodPosition.y-p->rodLength*p->rodOrientation.y*0.5;
    end.x=p->rodPosition.x+p->rodLength*p->rodOrientation.x*0.5;
    end.y=p->rodPosition.y+p->rodLength*p->rodOrientation.y*0.5;
    drawLineColorFast(image,begin,end,1.0);

    // draw also the width
    com.x=p->rodPosition.x;
    com.y=p->rodPosition.y;

    rodperp.x = p->rodOrientation.y*-1.0;
    rodperp.y = p->rodOrientation.x;

    pb.x=com.x-0.5*p->rodWidth*rodperp.x;
    pb.y=com.y-0.5*p->rodWidth*rodperp.y;
    pe.x=com.x+0.5*p->rodWidth*rodperp.x;
    pe.y=com.y+0.5*p->rodWidth*rodperp.y;
    drawLineColorFast(image,pb,pe,1.0);
  }
}

/* Function: delOverlap
   Checks for overlap for a pair of particles. If found, it removes the shortest one from an overlapping pair. 

   Parameters:
     box - the box struct
*/
void delOverlap(tBox *box)
{
  int i,j;
  int cn=0;
  int cn2=0;
  int cn3=0;
  int npart=0;
  tBackbone *p=box->bpart;
  tBackbone *q=NULL;
  float ra1[3],ra2[3];
  float rb1[3],rb2[3];
  float dist;

  for(i=0;p!=NULL;i++,p=p->next) {
    npart++;
    if(!p->del) {
    ra1[0]=p->rodPosition.x;
    ra1[1]=p->rodPosition.y;
    ra1[0]-=p->rodOrientation.x*p->rodLength*0.5; ra1[1]-=p->rodOrientation.y*p->rodLength*0.5; ra1[2]=0.0;
    ra2[0]=p->rodPosition.x;
    ra2[1]=p->rodPosition.y;
    ra2[0]+=p->rodOrientation.x*p->rodLength*0.5; ra2[1]+=p->rodOrientation.y*p->rodLength*0.5; ra2[2]=0.0;
    q=p->next;
    for(j=1;q!=NULL;j++,q=q->next) if(!q->del) {
      rb1[0]=q->rodPosition.x;    rb1[1]=q->rodPosition.y;    
      rb1[0]-=q->rodOrientation.x*q->rodLength; rb1[1]-=q->rodOrientation.y*q->rodLength; rb1[2]=0.0;
      rb2[0]=q->rodPosition.x;    rb2[1]=q->rodPosition.y;    
      rb2[0]+=q->rodOrientation.x*q->rodLength; rb2[1]+=q->rodOrientation.y*q->rodLength; rb2[2]=0.0;
      dist=distanceLineLine(ra1,ra2,rb1,rb2);

      if(dist/box->diameter < OVERLAPR){ // remove the one that seems less nice THB 04-09-2012
        cn++;
        if(p->length < q->length) {p->del=1;cn2++;} else {q->del=1;cn3++;}
      }
    }
   }
 }

  tBackbone *t=box->bpart;
  int n=0;
  for(i=0;t!=NULL;i++,t=t->next) if(!t->del) n++;
  box->nn=n;
}

/* Function: delBadFits
   Marks all particles sticking out of the frame and all particles containing NAN as deleted.  

   Parameters:
     box - the box struct
     frame - frame number
*/
void delBadFits(tBox *box, int frame)
{
  tBackbone *p;
  for(p=box->bpart;p!=NULL;p=p->next) {
    if ( (p->rodPosition.x+p->rodLength*p->rodOrientation.x*0.5) < 0.0 ||
         (p->rodPosition.y+p->rodLength*p->rodOrientation.y*0.5) < 0.0 ||
         (p->rodPosition.x+p->rodLength*p->rodOrientation.x*0.5) > box->image->width ||
         (p->rodPosition.y+p->rodLength*p->rodOrientation.y*0.5) > box->image->height)  p->del = 1;

    if ( (p->rodPosition.x-p->rodLength*p->rodOrientation.x*0.5) < 0.0 ||
         (p->rodPosition.y-p->rodLength*p->rodOrientation.y*0.5) < 0.0 ||
         (p->rodPosition.x-p->rodLength*p->rodOrientation.x*0.5) > box->image->width ||
         (p->rodPosition.y-p->rodLength*p->rodOrientation.y*0.5) > box->image->height)  p->del = 1;

    if (isnan (p->rodPosition.x) || isnan (p->rodPosition.y) || isnan (p->rodOrientation.x) || isnan(p->rodOrientation.y) )
      p->del = 1;
  }
}

/* Function: saveCoords
   Saves the coordinates to the file specified in the global variable output.

   Parameters:
     box - the box struct
     frame - frame number
*/
void saveCoords(tBox *box, int frame)
{
  FILE *fp;
  if(strlen(POSITIONSFILE) > 0) 
  {
    if(DEBUG>1) printf("Writing coordinates to file: %s\n",POSITIONSFILE);
    fp = fopen(POSITIONSFILE,"a");
    if(fp==NULL) 
    {
        printf("ERROR: Failed to open output file!\n");
        exit(666);
    }
  } else fp = stdout;

  int n=0;
  tBackbone *p;
  for(p=box->bpart;p!=NULL;p=p->next) 
	if(!p->del) 
		n++;

  float version = -10.0;
  fprintf(fp,"&%i &%i\n",frame , n);
  fprintf(fp,"%f %f %f\n", box->size.x, box->size.y, version);
  
  for(p=box->bpart;p!=NULL;p=p->next) 
  {
    if(!p->del) 
    {
      float xpos_com = p->rodPosition.x; // Center of mass
      float ypos_com = p->rodPosition.y; 
      float xpos_end = p->rodPosition.x+p->rodLength*p->rodOrientation.x*0.5;
      float ypos_end = p->rodPosition.y+p->rodLength*p->rodOrientation.y*0.5;

      fprintf(fp,"%f %f %f %f %f %f %f %f %f %f %f %f\n", xpos_com, ypos_com, 0.0, xpos_end, ypos_end, 0.0, p->ar, p->ar2, p->ar3, p->rodWidth, p->rodLength, p->accuracy);
    }
  }
  fclose(fp);
}

/* Function: cpImage
   Creates and returns an exact copy of src.

   Parameters:
     src - source image
   
   Returns:
     res - resulting image
*/
tImage_uni *cpImage(tImage_uni *src)
{
  tImage_uni *res= (tImage_uni*) malloc(sizeof(tImage_uni));
  res->width=src->width;
  res->height=src->height;
  res->bitmap=(float*) malloc(sizeof(float)*res->width*res->height);
  memcpy(res->bitmap,src->bitmap,sizeof(float)*res->width*res->height);
  return res;
}

/* Function: cpImage
   Creates an empty copy of an image.

   Parameters:
     src - source image
   
   Returns:
     res - resulting image
*/
tImage_uni *cpImageEmpty(tImage_uni *src)
{
  tImage_uni *res= (tImage_uni*) malloc(sizeof(tImage_uni));
  res->width=src->width;
  res->height=src->height;
  res->bitmap=(float*) malloc(sizeof(float)*res->width*res->height);
  int i;
  for(i=0;i<res->width*res->height;i++) res->bitmap[i]=0.0;
  return res;
}


/* Function: countValid
   Counts the number of non-deleted particles.

   Parameters:
     box - the box struct
   
   Returns:
     v - the number of non-deleted particles
*/
int countValid(tBox *box)
{
  int v = 0;
  tBackbone *p;
  for(p=box->bpart;p!=NULL;p=p->next) {
    if(!p->del) v++;
  }
  return v;
}

/* Function: makeOverlay
   Makes and saves an image in which the found rods are overlayed on the input image

   Parameters:
     box - the box struct
     img - pointer to image
     i - frame number
*/
void makeOverlay(tBox *box,tImage_uni *img ,int i)
{
  if(DEBUG>1) printf("Making overlay image. \n");
  tImage_uni *overlay2= cpImageEmpty(img);

  if(DEBUG>=10) printf("Drawing rods. \n");
  drawRods(box,overlay2);

  if(DEBUG>=10) printf("Write rods image. \n");

  char buffer[1024];

  snprintf(buffer, sizeof(buffer), "%sFound_%.05i.tif", OUTPUTDIR, i);

  writeTiffRGB(buffer,img,overlay2,NULL);
  //writeTiff(buffer,img);

  free(overlay2->bitmap);
  free(overlay2);
}


/* Function: findRods
   Finds rods in image, performs fits, and appends coordinates to an output file

   Parameters:
     img - pointer to image
     i - frame number
*/
void findRods(tImage_uni img, int i)
{
  tBox *box;              //The box containing the coordinates
  box=(tBox*) malloc(sizeof(tBox));
  tImage_uni fil;    //The raw and the filtered image
  tImage_uni shapefil;

  float vdiameter;       // Particle diameter in x and y direction
  vdiameter=DIAMETER;    // Set with diameter estimate given in input file

  setupBox(box,vdiameter,img.width,img.height); //Initialize the box
  box->MINVAL=MINVAL;                      // The minimum value a local maximum should have

  subBackground(SUBBACKGROUND,&img);      // Subtract the noise level

  fil.width=img.width;
  fil.height=img.height;
  shapefil.width=img.width;
  shapefil.height=img.height;

  fil.bitmap= (float*) malloc(fil.width*fil.height*sizeof(float));

  if (RGB) {
    fil.r= (float*) malloc(fil.width*fil.height*sizeof(float));
    fil.g= (float*) malloc(fil.width*fil.height*sizeof(float));
    fil.b= (float*) malloc(fil.width*fil.height*sizeof(float));
  }

  int y,x;
  for(y=0;y<fil.height;y++) for(x=0;x<fil.width;x++) {
    *(fil.bitmap +x+y*fil.width) = *(img.bitmap +x+y*fil.width);
    if (RGB) {
      *(fil.r +x+y*fil.width) = *(img.r +x+y*fil.width);
      *(fil.g +x+y*fil.width) = *(img.g +x+y*fil.width);
      *(fil.b +x+y*fil.width) = *(img.b +x+y*fil.width);
    }
  }

  if(DEBUG>1) writeTiff("blur.tif",&fil);

  box->image=&fil;                     //Set the right image
  box->shapeimage=&shapefil;                     //Set the right image
  tImage_uni *filOrig=cpImage(&fil);

  if(DEBUG>1) printf("Searching for backbones.\n");
  findBackbones(box);

  if(DEBUG>1) printf("Connecting backbones.\n");
  connectBackbone(box);

  if(DEBUG>1) printf("Fitting backbones.\n");
  fitBackbones(box);

  if(DEBUG>1) printf("Found %i backbones.\n",box->n_part);

  box->image=&img;

  int loopr = 1; //hack

  int pr=0;
  for (pr = 0; pr < loopr; pr++) {
    if(DEBUG>1) printf("Refining. \n");
    fitBackboneRefine(box);  //Some particles will be deleted
    if(DEBUG>1) printf("%i local maxima after fitting and refining step no. %i\n",countValid(box), pr);
  }

  delOverlap(box); //Throws away overlapping particles based on length
  if(DEBUG>1) printf("%i local maxima after overlap filter.\n",countValid(box));

  if ( (i % WRITEFREQ) == 0) {
    makeOverlay(box,&img,i);
  } 

  if(DEBUG>1)  printf("Total number of particles found %i.\n",countValid(box));

  delBadFits(box,i);
  saveCoords(box, i);

  free(filOrig->bitmap);
  free(filOrig);
  free(fil.bitmap);
  if (RGB) 
  {
    free(fil.r);
    free(fil.g);
    free(fil.b);
  }

  tBackbone *p;
  for(p=box->bpart;p!=NULL;) 
  {
    tBackbone *q=p;
    p=p->next;
    free(q);
  }
  free(box);
  return;
}


/* Function: saveBackgroundImages
   Saves two composite background images as tiff files

   Parameters:
     images_bg - pointer to background image
     images_bg_blur - pointer to blurred background image
*/
void saveBackgroundImages(tImage_uni *images_bg,tImage_uni *images_bg_blur){
  char buffer[1024];
  snprintf(buffer, sizeof(buffer), "%sbackground.tif", OUTPUTDIR);
  writeTiff(buffer, images_bg);

  snprintf(buffer, sizeof(buffer), "%sbackground_blurred.tif", OUTPUTDIR);
  writeTiff(buffer, images_bg_blur);
}

/* Function: saveFrame
   Saves an image of the frame (after filtering steps, but without added information in it)

   Parameters:
     img - pointer to image
     i - frame number
*/
void saveFrame(tImage_uni *img,int i){
  char buffer[1024];
  snprintf(buffer, sizeof(buffer), "%sFrame_%.05i.tif", OUTPUTDIR, i);
  if ( (i % WRITEFREQ) == 0) {
    if (!RGB)
      writeTiff(buffer, img);
    else
      writeTiffRGBNormal(buffer, img);
  }
}

/* Function: writeHeader
   Writes the header of a file

   Parameters:
     n_frames - number of frames
*/
void writeHeader(int n_frames){
  FILE * fp;
  fp = fopen(POSITIONSFILE,"w");
  fprintf(fp,"&%i\n", n_frames);
  fclose(fp);
}

int main(int argc, char *argv[])
{
  //Read parameter file
  GetNameList(argc,argv);
  if (DEBUG)
	PrintNameList (stdout);

  tImage_uni *images; // The image
  tImage_uni *images_bg; // The image

  //Searching for image files
  glob_t globbuf;
  glob_t globbuf_bg;
  if(DEBUG) printf("Searching for images: %s\n",FILEMASK);
  if(glob(FILEMASK,GLOB_TILDE,NULL,&globbuf) != 0) printf("WARNING: Failed reading %s\n",FILEMASK);
  if(globbuf.gl_pathc < 1) {printf("ERROR: No files found\n"); exit(666);}

  if(DEBUG) printf("Searching for background images: %s\n", FILEMASKBG);
  if(glob(FILEMASKBG,GLOB_TILDE,NULL,&globbuf_bg) != 0) {
    fprintf(stderr,"WARNING: Failed reading background files %s\n",FILEMASKBG);
  }
  if(globbuf_bg.gl_pathc < 1) { // if no background files are present
    fprintf(stderr,"WARNING: No background files found\n");
  }

  /////////////////////////////////////////// Reading the background data ////////////////////////////////
  //
  images = (tImage_uni*) malloc(sizeof(tImage_uni)*globbuf.gl_pathc);

  tImage_uni images_bg_blur;
  if (globbuf_bg.gl_pathc > 0) { //if background files found read them 
    if(DEBUG) {
      printf("---------------------------------------------\n");
      printf("Reading Background frames.\n");
    }
    images_bg = (tImage_uni*) malloc(sizeof(tImage_uni)*1);
    readTiffBackground(globbuf_bg.gl_pathv, globbuf_bg.gl_pathc, images_bg, BGTHRESHOLD, WHITE, DEBUG);  // Read the tiff images
    blurIntense(images_bg, &images_bg_blur, 1.0);
    saveBackgroundImages(images_bg,&images_bg_blur);
    free(images_bg->bitmap);
    free(images_bg);
  }

  /////////////////////////////////////////// Reading the main data ////////////////////////////////
  if (DEBUG) {
    printf("---------------------------------------------\n");
    printf("Reading frames.\n");
  }

  /////////////////////////////////////////// Searching for rod shaped objects ////////////////////////////////
  writeHeader((int) globbuf.gl_pathc);

  int start = 0;
  int stop = 0;
  for (start = 0; start < globbuf.gl_pathc; start = start + CHUNK)
  {
	stop = start + CHUNK;
	if (stop > globbuf.gl_pathc)
		stop = globbuf.gl_pathc;

  	if (RGB == 0)
	    readTiffTimeSeries(globbuf.gl_pathv, start, stop, images, WHITE, DEBUG);  //Read the tiff image
	else
	    readTiffTimeSeriesRGB(globbuf.gl_pathv, start, stop, images, WHITE, DEBUG);  //Read the tiff image

  	/////////////////////////////////////////// Filtering the main data ////////////////////////////////
  	if (DEBUG) {
    		printf("---------------------------------------------\n");
    		printf("Processing and tophat filter..\n");
  	}  

  	if (globbuf_bg.gl_pathc > 0)
	{
    		process(start, stop, images, &images_bg_blur, TOPHATDIAMETER, BLURDIAMETER, WHITE, RGB, OUTPUTDIR, DEBUG);
  	} 
	else 
        {
    		process(start, stop, images, NULL, TOPHATDIAMETER, BLURDIAMETER, WHITE, RGB, OUTPUTDIR, DEBUG);
  	}

  	if (DEBUG) 
	{
	    printf("---------------------------------------------\n");
	    printf("Searching for the positions and orientations of the rods.\n");
  	}  
  	
	int i = start;
  	for (i = start; i < stop; i++) 
  	{
    		if(DEBUG > 2) 
    		{ 
      			printf("---------------------------------------------- \n");
      			printf("Working on frame %i/%lu\n", i,globbuf.gl_pathc);
    		}
    		tImage_uni *img = images+i;

    		if ( (i % WRITEFREQ) == 0) saveFrame(img,i);

    		if (FINDRODS)  findRods(*img, i);

    		fflush(stdout);
    		free(img->bitmap);
  	}

  	if(DEBUG) 
	{ 
    		printf("---------------------------------------------\n");
    		printf("Finished.\n");
  	}
  }
  printf("Closing..\n");  
  if (globbuf_bg.gl_pathc > 0)
  	free(images_bg_blur.bitmap);
  free(images);
  globfree(&globbuf);
  if (globbuf_bg.gl_pathc > 0)
  	globfree(&globbuf_bg);
  return 0;
}
