#!/bin/bash

echo "FILEMASK ../testcase/input/image00*.tif" > ./findRods2Dt.in
echo "FILEMASKBG ../testcase/background/*.tif" >> ./findRods2Dt.in
echo "POSITIONSFILE ../testcase/output/positions.dat" >> ./findRods2Dt.in
echo "OUTPUTDIR ../testcase/output/" >> ./findRods2Dt.in
echo "FINDRODS 1" >> ./findRods2Dt.in
echo "MINVAL 0.25" >> ./findRods2Dt.in
echo "DIAMETER 2.8" >> ./findRods2Dt.in
echo "BLURDIAMETER 0.8" >> ./findRods2Dt.in
echo "SUBBACKGROUND 0.0000001" >> ./findRods2Dt.in
echo "BGTHRESHOLD 0.40" >> ./findRods2Dt.in
echo "DEBUG 1" >> ./findRods2Dt.in
echo "OVERLAPR 1.0" >> ./findRods2Dt.in
echo "TOPHATDIAMETER 15" >> ./findRods2Dt.in
echo "MININTENS 5" >> ./findRods2Dt.in
echo "WRITEFREQ 1" >> ./findRods2Dt.in
echo "TRACERR 2" >> ./findRods2Dt.in
echo "WHITE 0" >> ./findRods2Dt.in
echo "RGB 0" >> ./findRods2Dt.in
echo "CHUNK 500" >> ./findRods2Dt.in

./findRods2Dt
