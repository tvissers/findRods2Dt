/*
    This file is part of findRods2Dt.

    findRods2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    findRods2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with findRods2Dt.  If not, see <http://www.gnu.org/licenses/>.
*/

// Functions in this file are for reading and writing tiff images.

#include <stdio.h>
#include <tiffio.h>
#include <stdlib.h>
#include <glob.h>
#include "tiffio.h"
#include "filter.h"

/* Function: readTiffBackground
   Reads a set of images, and removes background signals that are present in all the images (e.g. from dirt particles on the optics). 

   Parameters:

     filenames - list of image files
     nfiles - number of files
     images - pointer to images
     bgthreshold - threshold for a point to be tagged
     bacteriaAreWhite - set to 1 signal is high intensity (white), or to 0 for low intensity (black) 
     DEBUG - set to 0 for minimal output, 1 or higher to print extra DEBUG information
*/

void readTiffBackground(char **filenames, int nfiles, tImage_uni *images, double bgthreshold, int bacteriaAreWhite, int DEBUG)
{
  unsigned int width=0,height=0,depth=0;
  unsigned int x,y;
  int i;
  glob_t globbuf;
  if (DEBUG)
	  printf("Background series of %i files..\n", nfiles);

  for(i=0;i<nfiles;i++){

    if(i==0)
    {
      if(glob(filenames[i],GLOB_TILDE,NULL,&globbuf) != 0) {
        printf("Error reading %s \n",filenames[i]);
        exit(666);
      }
    } else {
      if(glob(filenames[i],GLOB_TILDE | GLOB_APPEND,NULL,&globbuf) != 0) {
        printf("Error reading %s \n",filenames[i]);
        exit(666);
      }
    }
  }
  depth=globbuf.gl_pathc;
  if (DEBUG)
	  printf("Start reading %i files..\n", depth);

  tImage_raw *raw = (tImage_raw*) malloc(sizeof(tImage_raw));
  tImage_uni* img;
  img = images; // i-th image from the series "images"
  
  if(globbuf.gl_pathc > 0) 
  {
    for(i=0;i<(int) globbuf.gl_pathc;i++) 
    { // looping over the files in the file-list   

      readTiffRaw(globbuf.gl_pathv[i],raw); // read file number i
      if(i==0)
      {
        width=raw->width;
        height=raw->height;
        img->width=width;
        img->height=height;
        img->bitmap=(float*) malloc(sizeof(float)*width*height);
      }

      if(raw->width != width || raw->height != height)
      {
        fprintf(stderr,"ERROR: Images not of the same size!\n");
        exit(666);
      }
  
      double max = 0.0; 
      float pixelvalue;
      
      // this code is to determining the maximum pixelvalue:
      for(y=0;y<height;y++) for(x=0;x<width;x++)
      {
         if(raw->bpp==1) // 8-bit image
                     {
                 if (bacteriaAreWhite)   
                         pixelvalue = ((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(255.0);
           else
                         pixelvalue = 1.0-((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(255.0); // invert image
          
                       if (pixelvalue > max)
                                  max = pixelvalue;
         }
         else if(raw->bpp==2) // 16-bit image
         {
           if (bacteriaAreWhite)
           {
                      pixelvalue=(((float) *(raw->bitmap+(1+x*raw->bpp+y*width*raw->bpp)))/(255.0));
                      pixelvalue+=(((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(65535.0));
           }
           else
           {
		      pixelvalue=(1.0-((float) *(raw->bitmap+(1+x*raw->bpp+y*width*raw->bpp)))/(255.0));
                      pixelvalue+=(1.0-((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(65535.0));
           }

                      if (pixelvalue > max)
                      	max = pixelvalue;
         }
      }

      // in the following a composite image of all background images is constructed:
      if (i == 0) // if this is the first image
      {
                        if(raw->bpp==1) // 8-bit image
                        {
                              for(y=0;y<height;y++) for(x=0;x<width;x++)
                              {
            if (bacteriaAreWhite)
                      pixelvalue = (((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(255.0));
        else
                      pixelvalue = (1.0-((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(255.0));
        
                                *(img->bitmap + x + y*width) = pixelvalue;
                              }
                        }
                        else if(raw->bpp==2) // 16-bit image
                        {
                                for(y=0;y<height;y++) for(x=0;x<width;x++)
                                {
          if (bacteriaAreWhite)
          {
                                          pixelvalue=(((float) *(raw->bitmap+(1+x*raw->bpp+y*width*raw->bpp)))/(255.0));
                                          pixelvalue+=(((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(65535.0));
          }
          else
          {
                                          pixelvalue=(1.0-((float) *(raw->bitmap+(1+x*raw->bpp+y*width*raw->bpp)))/(255.0));
                                          pixelvalue+=(1.0-((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(65535.0));
          }
                                        *(img->bitmap + x + y*width) = pixelvalue;
                                }
                        }
      }
      else // for all following images, add pixel values.
      {
          if(raw->bpp==1) // 8-bit image
          {
                for(y=0;y<height;y++) for(x=0;x<width;x++)
                {
          		if (bacteriaAreWhite)
                        	pixelvalue = (((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(255.0));
          		else
                        	pixelvalue = (1.0-((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(255.0));

                        *(img->bitmap + x + y*width) += pixelvalue;
          	}
          }
          else if(raw->bpp==2) // 16-bit image
          {
          	for(y=0;y<height;y++) for(x=0;x<width;x++)
                {
          		if (bacteriaAreWhite)
          		{
                        	pixelvalue=(((float) *(raw->bitmap+(1+x*raw->bpp+y*width*raw->bpp)))/(255.0));
                                pixelvalue+=(((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(65535.0));
          		}
          		else
          		{
                        	pixelvalue=(1.0-((float) *(raw->bitmap+(1+x*raw->bpp+y*width*raw->bpp)))/(255.0));
                                pixelvalue+=(1.0-((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(65535.0));
          		}
                                        *(img->bitmap + x + y*width) += pixelvalue;
            	}
          }
      }

      _TIFFfree(raw->bitmap); // freeing the memory of the raw bitmap image
    }

    // divide each pixelvalue by the number of background images
    double numberOfBackgroundImages = (double)globbuf.gl_pathc;
    for(y=0;y<height;y++) {
      for(x=0;x<width;x++) {
        *(img->bitmap + x + y*width) = *(img->bitmap + x + y*width)/numberOfBackgroundImages;
      }
    }

    double max = 0.0;
    for(y=0;y<height;y++) {
      for(x=0;x<width;x++) {
        double pixelvalue = *(img->bitmap + x + y*width);
        if (pixelvalue > max) 
          max = pixelvalue;
      }
    }

    
    tImage_uni img_temp;

    float sigmax=15.0;
    float sigmay=15.0;

    tophatBackground(img, &img_temp, sigmax, sigmay);

    for(y=0;y<height;y++) {
      for(x=0;x<width;x++) {
        *(img->bitmap + x + y*width) = *(img_temp.bitmap + x + y*width);
      }
    }

    free(img_temp.bitmap);

    max = 0.0;
    for(y=0;y<height;y++) {
      for(x=0;x<width;x++) {
        double pixelvalue = *(img->bitmap + x + y*width);
        if (pixelvalue > max)
          max = pixelvalue;
      }
    }

    for(y=0;y<height;y++) {
      for(x=0;x<width;x++) {
        *(img->bitmap + x + y*width) = *(img->bitmap + x + y*width) /max;
      }
    }
 
    tImage_uni* img_tmp = (tImage_uni*) malloc(sizeof(tImage_uni)*1);
    img_tmp->width=width;
    img_tmp->height=height;
    img_tmp->bitmap=(float*) malloc(sizeof(float)*width*height);

    
    // Makes pixels in img_temp->bitmap white if in img->bitmap they have a value greater than threshold 
    // (if this is the case, the signal occurs frequently in background images). 
    // Else, pixel in img_temp->bitmap will be set to black.
   
    for(y=0;y<height;y++) {
      for(x=0;x<width;x++) {
        if (y > 0 && y < (height -1) && x > 0 && x < (width-1) ) {
          if (*(img->bitmap + x + y*width) < bgthreshold*max) { // cutoff
            *(img_tmp->bitmap + x + y*width) = 0.0;
          } else {
            *(img_tmp->bitmap + x + y*width) = 1.0; //*(img->bitmap + x + y*width)/max;
            *(img_tmp->bitmap + x + (y+1)*width) = 1.0; //*(img->bitmap + x + y*width)/max;
            *(img_tmp->bitmap + x + (y-1)*width) = 1.0; //*(img->bitmap + x + y*width)/max;

            *(img_tmp->bitmap + (x+1) + y*width) = 1.0; //*(img->bitmap + x + y*width)/max;
            *(img_tmp->bitmap + (x+1) + (y+1)*width) = 1.0; //*(img->bitmap + x + y*width)/max;
            *(img_tmp->bitmap + (x+1) + (y-1)*width) = 1.0; //*(img->bitmap + x + y*width)/max;

            *(img_tmp->bitmap + (x-1) + y*width) = 1.0; //*(img->bitmap + x + y*width)/max;
            *(img_tmp->bitmap + (x-1) + (y+1)*width) = 1.0; //*(img->bitmap + x + y*width)/max;
            *(img_tmp->bitmap + (x-1) + (y-1)*width) = 1.0; //*(img->bitmap + x + y*width)/max;
          }
        } else {
          *(img_tmp->bitmap + x + y*width) = 0.0;
        }
      }
    }

    for(y=0;y<height;y++) {
      for(x=0;x<width;x++) {
        *(img->bitmap + x + y*width) = *(img_tmp->bitmap + x + y*width);
      }
    }

    free(img_tmp->bitmap);
    free(img_tmp);
    
  }

  free(raw);
  globfree(&globbuf);

  if (DEBUG)
  	printf("Memory for images freed..\n");
}

/* Function: readTiffTimeSeries
   Reads a set of monochrome images for a time series 

   Parameters:

     filenames - list of image files
     nfiles - number of files
     images - pointer to images
     bacteriaAreWhite - set to 1 signal is high intensity (white), or to 0 for low intensity (black) 
     DEBUG - set to 0 for minimal output, 1 or higher to print extra DEBUG information
*/

void readTiffTimeSeries(char **filenames, int start, int stop, tImage_uni *images, int bacteriaAreWhite, int DEBUG)
{
  if (DEBUG)
  	printf("time series from frame %i to frame %i..\n", start, stop);

  unsigned int width,height,depth;
  unsigned int x,y;
  int i;
  glob_t globbuf;

  for(i=start;i<stop;i++){
   
    //printf("%s\n", filenames[i]);

    if(i==start) 
    {
      if(glob(filenames[i],GLOB_TILDE,NULL,&globbuf) != 0) {
        printf("Error reading %s \n",filenames[i]);
        exit(666);
      }
    } else {
      if(glob(filenames[i],GLOB_TILDE | GLOB_APPEND,NULL,&globbuf) != 0) {
        printf("Error reading %s \n",filenames[i]);
        exit(666);
      }
    }
  }

  depth=globbuf.gl_pathc;
  if (DEBUG)
	  printf("start reading %i files..\n", depth);

  double divisor = 1.0/255.0;
 
  tImage_raw *raw = (tImage_raw*) malloc(sizeof(tImage_raw));
  tImage_uni* img;
  if(globbuf.gl_pathc > 0)
  {
    for(i=0;i<(int) globbuf.gl_pathc;i++)
    { // looping over the files in the file-list   
      
      img = images+i+start; // i-th image from the series "images"

      readTiffRaw(globbuf.gl_pathv[i],raw); // read file number i
      width=raw->width;
      height=raw->height;
      img->width=width;
      img->height=height;
      img->bitmap=(float*) malloc(sizeof(float)*width*height);
    
      if(raw->width != width || raw->height != height) 
      {
        fprintf(stderr,"ERROR: Images not of the same size!\n");
        exit(666);
      }
      
      if(raw->bpp==1) // 8-bit
      {
        for(y=0;y<height;y++) for(x=0;x<width;x++)
        {
                *(img->bitmap + x + y*width)=((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))*divisor;
        }
      }
      else if(raw->bpp==2) // 16-bit
      {
        for(y=0;y<height;y++) for(x=0;x<width;x++)
        {
          *(img->bitmap + x + y*width)=((float) *(raw->bitmap+(1+x*raw->bpp+y*width*raw->bpp)))/(255.0);
          *(img->bitmap + x + y*width)+=((float) *(raw->bitmap+(x*raw->bpp+y*width*raw->bpp)))/(65535.0);
        }
      }

      _TIFFfree(raw->bitmap); // freeing the memory of the raw bitmap image
    }
  }
  free(raw);
  globfree(&globbuf);
  if (DEBUG)
  	printf("Memory for images freed..\n");
}

/* Function: readTiffTimeSeriesRGB
   Reads a set of RGB images for a time series 

   Parameters:

     filenames - list of image files
     nfiles - number of files
     images - pointer to images
     bacteriaAreWhite - set to 1 signal is high intensity (white), or to 0 for low intensity (black) 
     DEBUG - set to 0 for minimal output, 1 or higher to print extra DEBUG information
*/
void readTiffTimeSeriesRGB(char **filenames, int start, int stop, tImage_uni *images, int bacteriaAreWhite, int DEBUG)
{
  if (DEBUG)
  {
	printf("read RGB series\n");
  }  

  //printf("time series of %i files..\n", nfiles);
  unsigned int width,height,depth;
  unsigned int x,y;
  int i;
  glob_t globbuf;
  for(i=start;i<stop;i++)
  {
    if(i==0)
    {
      if(glob(filenames[i],GLOB_TILDE,NULL,&globbuf) != 0) {
        printf("Error reading %s \n",filenames[i]);
        exit(666);
      }
    } else {
      if(glob(filenames[i],GLOB_TILDE | GLOB_APPEND,NULL,&globbuf) != 0) {
        printf("Error reading %s \n",filenames[i]);
        exit(666);
      }
    }
  }
  depth=globbuf.gl_pathc;
  
  if (DEBUG)
  	printf("start reading %i files..\n", depth);

  tImage_raw *raw = (tImage_raw*) malloc(sizeof(tImage_raw));
  tImage_uni* img;

  if(globbuf.gl_pathc > 0)
  {
    //for(i=0;i<globbuf.gl_pathc && i < 2000;i++)
    for(i=0;i< (int) globbuf.gl_pathc;i++)
    { // looping over the files in the file-list   

      img = images+i+start; // i-th image from the series "images"
      if (DEBUG)
      	printf("Reading %s\n", filenames[i]);

      readTiffRawRGB(globbuf.gl_pathv[i],raw); // read file number i
      width=raw->width;
      height=raw->height;
      img->width=width;
      img->height=height;
      img->r=(float*) malloc(sizeof(float)*width*height);
      img->g=(float*) malloc(sizeof(float)*width*height);
      img->b=(float*) malloc(sizeof(float)*width*height);
      img->bitmap=(float*) malloc(sizeof(float)*width*height);

      raw->bpp=4;
      if(raw->width != width || raw->height != height)
      {
        fprintf(stderr,"ERROR: Images not of the same size!\n");
        exit(666);
      }
        for(y=0;y<height;y++) for(x=0;x<width;x++)
        {
                *(img->r + x + (height-y-1)*width)=((float) *(raw->bitmap+(0+x*raw->bpp+y*width*raw->bpp)))/(255.0);
                *(img->g + x + (height-y-1)*width)=((float) *(raw->bitmap+(1+x*raw->bpp+y*width*raw->bpp)))/(255.0);
                *(img->b + x + (height-y-1)*width)=((float) *(raw->bitmap+(2+x*raw->bpp+y*width*raw->bpp)))/(255.0);
                *(img->bitmap + x + (height-y-1)*width) = 0.0;
    //printf("[]--> %f %f %f\n", *(img->r + x + (height-y-1)*width), *(img->g + x + (height-y-1)*width), *(img->b + x + (height-y-1)*width) );
        }

      _TIFFfree(raw->bitmap); // freeing the memory of the raw bitmap image
    }
  }
  free(raw);
  if (DEBUG)
  	printf("memory freed..\n");
}

/* Function: readTiff 
   Reads a tiff file

   Parameters:

     filename - Point to image file
     img - pointer to image
*/

void readTiff(const char * filename, tImage_uni *img){
  tImage_raw *raw = (tImage_raw*) malloc(sizeof(tImage_raw));
  readTiffRaw(filename,raw);
  if(raw->bpp != 1) {
    fprintf(stderr,"Warning: Not a single channel image taking red.\n");
  }
  unsigned int x,y;
  img->height=raw->height;
  img->width=raw->width;
  img->bitmap = (float*) malloc(img->height*img->width*sizeof(float));
  double divisor = 1.0/255.0;
  for(y=0;y<raw->height;y++) for(x=0;x<raw->width;x++){
    *(img->bitmap + x + y*img->width)=((float) *(raw->bitmap+(x*raw->bpp+y*img->width*raw->bpp)))*divisor;
  }
  _TIFFfree(raw->bitmap);
  free(raw);
}

/* Function: writeTiff 
   Writes a tiff file

   Parameters:

     filename - pointer to output file name
     img - pointer to image
*/

void writeTiff(const char *filename, tImage_uni *img){
  tImage_raw *raw = (tImage_raw*) malloc(sizeof(tImage_raw));
  unsigned int i,j;
  raw->height=img->height;
  raw->width=img->width;
  raw->bpp=1;
  raw->bitmap = (unsigned char*) malloc(raw->height*raw->width*sizeof(unsigned char));

  for(j=0;j<raw->height;j++){
    for(i=0;i<raw->width;i++)
    {
      *(raw->bitmap + i + j*img->width)=(unsigned char) (*(img->bitmap+i+j*img->width)*255.0);
    }
  }

  writeTiffRaw(filename,raw);
  _TIFFfree(raw->bitmap);
  free(raw);
}

void writeTiffRGB(const char *filename, tImage_uni *imgr, tImage_uni *imgg, tImage_uni *imgb){
  tImage_raw *raw = (tImage_raw*) malloc(sizeof(tImage_raw));
  unsigned int i,j;
  raw->height=imgr->height;
  raw->width=imgr->width;
  raw->bpp=3;
  raw->bitmap = (unsigned char*) malloc(raw->height*raw->width*sizeof(char)*3);
  for(j=0;j<raw->height;j++){
    for(i=0;i<raw->width;i++){
      *(raw->bitmap + (i + j*imgr->width)*3+0)=(unsigned char) (*(imgr->bitmap+i+j*imgr->width)*255.0);
      if(imgg) *(raw->bitmap + (i + j*imgr->width)*3+1)=(unsigned char) (*(imgg->bitmap+i+j*imgr->width)*255.0);
      else     *(raw->bitmap + (i + j*imgr->width)*3+1)=(unsigned char) 0;
      if(imgb) *(raw->bitmap + (i + j*imgr->width)*3+2)=(unsigned char) (*(imgb->bitmap+i+j*imgr->width)*255.0);
      else     *(raw->bitmap + (i + j*imgr->width)*3+2)=(unsigned char) 0;
    }
  }
  writeTiffRaw(filename,raw);
  _TIFFfree(raw->bitmap);
  free(raw);
}

/* Function: writeTiffRGBCombined 
   Writes a tiff file containing the image plus RGB overlay information

   Parameters:

     filename - pointer to output file name
     imgr - pointer to image red channel
     imgg - pointer to image greeb channel
     imgb - pointer to image blue channel
*/
void writeTiffRGBCombined(const char *filename, tImage_uni *imgr, tImage_uni *imgg, tImage_uni *imgb){
  tImage_raw *raw = (tImage_raw*) malloc(sizeof(tImage_raw));
  unsigned int i,j;
  raw->height=imgr->height;
  raw->width=imgr->width;
  raw->bpp=3;
  raw->bitmap = (unsigned char*) malloc(raw->height*raw->width*sizeof(unsigned char)*3);
  for(j=0;j<raw->height;j++){
    for(i=0;i<raw->width;i++){
      *(raw->bitmap + (i + j*imgr->width)*3+0)=(unsigned char) (*(imgr->bitmap+i+j*imgr->width)*255.0);
      if(imgr) *(raw->bitmap + (i + j*imgr->width)*3+1)=(unsigned char) (*(imgr->bitmap+i+j*imgr->width)*255.0);
      else     *(raw->bitmap + (i + j*imgr->width)*3+1)=(unsigned char) 0;
      if(imgr) *(raw->bitmap + (i + j*imgr->width)*3+2)=(unsigned char) (*(imgr->bitmap+i+j*imgr->width)*255.0);
      else     *(raw->bitmap + (i + j*imgr->width)*3+2)=(unsigned char) 0;

      if(imgg)
      {
        if (*(imgg->bitmap+i+j*imgr->width) > 0.0 )
                *(raw->bitmap + (i + j*imgr->width)*3+1)=(unsigned char) (255.0);
      }
      if(imgb)
      {
        if (*(imgb->bitmap+i+j*imgr->width) > 0.0 )
                *(raw->bitmap + (i + j*imgr->width)*3+0)=(unsigned char) (255.0);
      }
    }
  }
  writeTiffRaw(filename,raw);
  _TIFFfree(raw->bitmap);
  free(raw);
}

/* Function: writeTiffRGBNormal 
   Writes an RGB tiff file

   Parameters:

     filename - pointer to output file name
     img - pointer to image
*/
void writeTiffRGBNormal(const char *filename, tImage_uni *img){
  tImage_raw *raw = (tImage_raw*) malloc(sizeof(tImage_raw));
  unsigned int i,j;
  raw->height=img->height;
  raw->width=img->width;
  raw->bpp=3;
  raw->bitmap = (unsigned char*) malloc(raw->height*raw->width*sizeof(unsigned char)*3);
  for(j=0;j<raw->height;j++){
    for(i=0;i<raw->width;i++)
    {
      *(raw->bitmap + (i + j*img->width)*3+0)=(unsigned char) (*(img->r+i+j*img->width)*255.0);
      if(img->g) 
  *(raw->bitmap + (i + j*img->width)*3+1)=(unsigned char) (*(img->g+i+j*img->width)*255.0);
      else     
  *(raw->bitmap + (i + j*img->width)*3+1)=(unsigned char) 0;
      if(img->r) 
  *(raw->bitmap + (i + j*img->width)*3+2)=(unsigned char) (*(img->b+i+j*img->width)*255.0);
      else     
  *(raw->bitmap + (i + j*img->width)*3+2)=(unsigned char) 0;
    }
  }
  writeTiffRaw(filename,raw);
  _TIFFfree(raw->bitmap);
  free(raw);
}

/* Function: readTiffRawRGB
   Reads a raw RGB tiff and puts it in img structure

   Parameters:

     filename - pointer to input file name
     img - pointer to image
*/
void readTiffRawRGB(const char * filename, tImage_raw *img){
  TIFF* tif = TIFFOpen(filename, "r");
  if (tif) {
    uint32 w, h;
    size_t npixels;

    if (TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w)==0 ) {
      fprintf(stderr,"Image does not have a width.\n");
      exit(42);
    }
    img->width=w;
    if (TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h)==0 ) {
      fprintf(stderr,"Image does not have a height.\n");
      exit(42);
    }
    img->height=h;
    npixels = w * h;
    img->bitmap = (unsigned char *) _TIFFmalloc(npixels * sizeof (uint32));
    img->bpp=4;
    if (img->bitmap != NULL) {
      if (!TIFFReadRGBAImage(tif, w, h, (uint32*) img->bitmap, 0)) {
        fprintf(stderr,"Error reading image: %s\n",filename);
        exit(42);
      }
    }
    TIFFClose(tif);
  } else {
    fprintf(stderr,"Error reading file %s \n",filename);
    exit(42);
  }
}

/* Function: readTiffRaw
   Reads a raw tiff and puts it in img structure

   Parameters:

     filename - pointer to input file name
     img - pointer to image
*/
void readTiffRaw(const char * filename,tImage_raw *img){
  TIFF* tif = TIFFOpen(filename, "r");
  if (tif) {
    uint32 w, h;
    uint16 bps, spp;

    if (TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w)==0 ) {
      fprintf(stderr,"ERROR: Image does not have a width.\n");
      exit(42);
    }
    img->width=w;
    if (TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h)==0 ) {
      fprintf(stderr,"ERROR: Image does not have a height.\n");
      exit(42);
    }
    img->height=h;
    if ((TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bps)==0) || !((bps == 8)||(bps == 16)) ) {
      fprintf(stderr,"ERROR: Can not read image with %u bits per sample!\n",bps);
      exit(42);
    }
    if ((TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &spp)==0)) {
      fprintf(stderr,"ERROR: Image does not have a valid number of samples per pixel.\n");
      exit(42);
    }
    img->bpp=bps*spp/8;
    
    unsigned int scanlinesize = TIFFScanlineSize(tif);
    img->bitmap = (unsigned char *) _TIFFmalloc(h*scanlinesize);
    if (img->bitmap != NULL) {
      unsigned int i;
      for(i=0;i<h;i++){
        if (!TIFFReadScanline(tif,(img->bitmap+i*scanlinesize),i,0) ) {
          fprintf(stderr,"Error reading image: %s\n",filename);
          exit(42);
        }
      }  
    }
    TIFFClose(tif);
  } else {
    fprintf(stderr,"Error reading file %s \n",filename);
    exit(42);
  }
}

/* Function: writeTiffRaw
   Writes a raw tiff

   Parameters:

     filename - pointer to output file name
     img - pointer to image
*/

void writeTiffRaw(const char * filename, tImage_raw *img){
  TIFF* tif = TIFFOpen(filename, "w");
  if (tif){
    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, img->width);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, img->height);
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, img->bpp);

    TIFFSetField(tif, TIFFTAG_COMPRESSION, 1); //No compression
    uint16 photo;
    if(img->bpp==1) photo = 1; /*b&w*/ else photo=2; /*rgb*/
    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, photo);
    TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);

    // Write the information to the file
    unsigned int i;
    for(i=0;i<img->height;i++){
      TIFFWriteScanline(tif, (uint32*) &(img->bitmap[img->width*i*img->bpp]),i,0);
    }  
    TIFFClose(tif);
  } else {
    fprintf(stderr,"Error reading file %s \n",filename);
    exit(42);
  }
}
