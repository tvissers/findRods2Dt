/*
    This file is part of findRods2Dt.

    findRods2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    findRods2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with findRods2Dt.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <tiffio.h>
#include <stdlib.h>

typedef struct {
  unsigned int width;
  unsigned int height;
  unsigned int bpp;
  unsigned char *bitmap;
} tImage_raw;

typedef struct {
  unsigned int width;
  unsigned int height;
  float *r;
  float *g;
  float *b;
} tImage_rgb;

typedef struct
{
  unsigned int width;
  unsigned int height;
  float *bitmap;
  float *r;
  float *g;
  float *b;
  float *tmp;
} tImage_uni;

typedef struct {
  unsigned int width;
  unsigned int height;
  float *bitmap;
} tImage;

typedef struct {
  unsigned int width;
  unsigned int height;
  unsigned int depth;
  float *bitmap;
} tImage3d;

void readTiffTimeSeries(char **filenames, int start, int stop, tImage_uni *img, int bacteriaAreWhite, int DEBUG);
void readTiffTimeSeriesRGB(char **filenames, int start, int stop, tImage_uni *img, int bacteriaAreWhite, int DEBUG);
void readTiffBackground(char **filenames, int nfiles, tImage_uni *img, double bgthreshold, int bacteriaAreWhite, int DEBUG);
void readTiff(const char * filename, tImage_uni *img);
void writeTiff(const char * filename, tImage_uni *img);
void writeTiffRGB(const char * filename, tImage_uni *imgr, tImage_uni *imgg, tImage_uni *imgb);
void writeTiffRGBNormal(const char * filename, tImage_uni *img);
void writeTiffRGBCombined(const char * filename, tImage_uni *imgr, tImage_uni *imgg, tImage_uni *imgb);
void readTiffRawRGB(const char * filename, tImage_raw *img);
void readTiffRaw(const char * filename,tImage_raw *img);
void writeTiffRaw(const char * filename, tImage_raw *img);
