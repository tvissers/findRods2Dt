/*
    This file is part of findRods2Dt.

    findRods2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    findRods2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with findRods2Dt.  If not, see <http://www.gnu.org/licenses/>.
*/

// Functions in this file are for filtering tiff images.

#include <stdio.h>
#include <tiffio.h>
#include <stdlib.h>
#include <math.h>
#include "tiffio.h"
#include "filter.h"

/* Function: blurRGB
   Blurs RGB image using a Gaussian kernel   

   Parameters:

     img - Pointer to image
     newImg - Pointer to image
     sigma - one standard deviation, used for Gaussian kernel
*/
void blurRGB(tImage_uni* img, tImage_uni* newImg, float sigma)
{
  int i,x,y;
  float som,t,t_r,t_g,t_b;
  int width=img->width,height=img->height;
  int radius = (int) ceil(sigma*3.0);
  int diameter=radius*2+1;
  tImage_uni *tmp = (tImage_uni*) malloc(sizeof(tImage_uni));

  //create an empty image
  tmp->width=width;
  tmp->height=height;
  tmp->bitmap= (float*) malloc(width*height*sizeof(float));

  // reserving memory
  tmp->r= (float*) malloc(width*height*sizeof(float));
  tmp->g= (float*) malloc(width*height*sizeof(float));
  tmp->b= (float*) malloc(width*height*sizeof(float));

  newImg->r= (float*) malloc(width*height*sizeof(float));
  newImg->g= (float*) malloc(width*height*sizeof(float));
  newImg->b= (float*) malloc(width*height*sizeof(float));

  newImg->width=width;
  newImg->height=height;
  /*
  newImg->bitmap= (float*) malloc(width*height*sizeof(float));
  */

  //create 1d Gaussian kernel for convolution

  float* kernel= (float*) malloc(diameter*sizeof(float));
  for(i=0;i<diameter;i++){
    kernel[i]=exp(-(i-radius)*(i-radius)/(2*sigma*sigma));
  }
  // Do the first convolution

  for(y=0;y<height;y++) for(x=0;x<width;x++)
  {
    t=0;
    t_r=0;
    t_g=0;
    t_b=0;
    som=0;
    for(i=0;i<diameter;i++)
    {
      if(x+(i-radius) > 0 && (x+(i-radius) < width))
      {
        t+=*(img->bitmap +x+(i-radius)+y*width)*kernel[i];
        som+=kernel[i];

        t_r+=*(img->r +x+(i-radius)+y*width)*kernel[i];
        t_g+=*(img->g +x+(i-radius)+y*width)*kernel[i];
        t_b+=*(img->b +x+(i-radius)+y*width)*kernel[i];
      }
    }
    *(tmp->bitmap +x+y*width)=t/som;
    *(tmp->r +x+y*width)=t_r/som;
    *(tmp->g +x+y*width)=t_g/som;
    *(tmp->b +x+y*width)=t_b/som;
  }

  // Do the second one 
  for(y=0;y<height;y++) for(x=0;x<width;x++)
  {
    t=0;
    t_r=0;
    t_g=0;
    t_b=0;
    som=0;
    for(i=0;i<diameter;i++)
    {
      if(y+(i-radius) > 0 && (y+(i-radius) < height))
      {
        t+=*(tmp->bitmap +x+(i-radius)*width+y*width)*kernel[i];
        som+=kernel[i];
        t_r+=*(tmp->r +x+(i-radius)*width+y*width)*kernel[i];
        t_g+=*(tmp->g +x+(i-radius)*width+y*width)*kernel[i];
        t_b+=*(tmp->b +x+(i-radius)*width+y*width)*kernel[i];
      }
    }
    *(newImg->bitmap +x+y*width)=t/som;
    *(newImg->r +x+y*width)=t_r/som;
    *(newImg->g +x+y*width)=t_g/som;
    *(newImg->b +x+y*width)=t_b/som;
  }

  // Freeing memory
  free(tmp->bitmap);

  free(tmp->r);
  free(tmp->g);
  free(tmp->b);

  free(tmp);
  free(kernel);
}

/* Function: blur
   Blurs a monochrome image using a Gaussian kernel   

   Parameters:

     img - Pointer to image
     newImg - Pointer to image
     sigma - one standard deviation, used for Gaussian kernel
*/
void blur(tImage_uni* img, tImage_uni* newImg, float sigma)
{
  
  int i,x,y;
  float som,t;
  int width=img->width,height=img->height;
  int radius = (int) ceil(sigma*3.0);
  int diameter=radius*2+1;
  newImg->width=width;
  newImg->height=height;

  if (sigma > 0.0)
  {
  	tImage_uni *tmp = (tImage_uni*) malloc(sizeof(tImage_uni));
  	//create an empty image
  	tmp->width=width;
	  tmp->height=height;
	  tmp->bitmap= (float*) malloc(width*height*sizeof(float));

	  /*
	  newImg->bitmap= (float*) malloc(width*height*sizeof(float));
	  */

	  //create 1d Gaussian kernel for convolution
	  float* kernel= (float*) malloc(diameter*sizeof(float));
	  for(i=0;i<diameter;i++){
	    kernel[i]=exp(-(i-radius)*(i-radius)/(2*sigma*sigma));
	  }

	  //Do the first convolution
	  for(y=0;y<height;y++) for(x=0;x<width;x++)
	  {
	    t=0;
	    som=0;
	    for(i=0;i<diameter;i++)
	    {
	      if(x+(i-radius) > 0 && (x+(i-radius) < width)) 
	      {
		t+=*(img->bitmap +x+(i-radius)+y*width)*kernel[i];
		som+=kernel[i];
	      }
	    }
	    *(tmp->bitmap +x+y*width)=t/som;
	  }

	  //Do the second one 
	  for(y=0;y<height;y++) for(x=0;x<width;x++)
	  {
	    t=0;
	    som=0;
	    for(i=0;i<diameter;i++)
	    {
	      if(y+(i-radius) > 0 && (y+(i-radius) < height)) 
	      {
		t+=*(tmp->bitmap +x+(i-radius)*width+y*width)*kernel[i];
		som+=kernel[i];
	      }
	    }
	    *(newImg->bitmap +x+y*width)=t/som;
	  }

	  free(tmp->bitmap);
	  free(tmp);
	  free(kernel);
  }
  else
  {
	// no blurring simply copy the original image
	for(y=0;y<height;y++) 
	{
		for(x=0;x<width;x++)
		{
            		*(newImg->bitmap +x+y*width)=*(img->bitmap +x+y*width);
          	}
	}
  }

}

/* Function: blurIntense
   Blurs RGB image using a Gaussian kernel   

   Parameters:

     img - Pointer to image
     newImg - Pointer to image
     sigma - one standard deviation, used for Gaussian kernel
*/
void blurIntense(tImage_uni* img, tImage_uni* newImg, float sigma)
{
  int i,x,y;
  float som,t;
  int width=img->width,height=img->height;
  int radius = (int) ceil(sigma*3.0);
  int diameter=radius*2+1;
  tImage_uni *tmp = (tImage_uni*) malloc(sizeof(tImage_uni));
 //create an empty image
  tmp->width=width;
  tmp->height=height;
  tmp->bitmap= (float*) malloc(width*height*sizeof(float));
  newImg->width=width;
  newImg->height=height;
  newImg->bitmap= (float*) malloc(width*height*sizeof(float));
 //create 1d Gaussian kernel for convolution
  float* kernel= (float*) malloc(diameter*sizeof(float));
  for(i=0;i<diameter;i++){
    kernel[i]=exp(-(i-radius)*(i-radius)/(2*sigma*sigma));
  }
 //Do the first convolution
  for(y=0;y<height;y++) for(x=0;x<width;x++){
    t=0;
    som=0;
    for(i=0;i<diameter;i++){
      if(x+(i-radius) > 0 && (x+(i-radius) < width)) {
        t+=*(img->bitmap +x+(i-radius)+y*width)*kernel[i];
        som+=kernel[i];
      }
    }
    *(tmp->bitmap +x+y*width)=t/som;
  }
 //Do the second one 
 double max = 0.0;
  for(y=0;y<height;y++) for(x=0;x<width;x++){
    t=0;
    som=0;
    for(i=0;i<diameter;i++){
      if(y+(i-radius) > 0 && (y+(i-radius) < height)) {
        t+=*(tmp->bitmap +x+(i-radius)*width+y*width)*kernel[i];
        som+=kernel[i];
      }
    }
    *(newImg->bitmap +x+y*width)=t/som;
    if (*(newImg->bitmap +x+y*width) > max)
        max = *(newImg->bitmap +x+y*width);
  }

  // renormalisation:
  for(y=0;y<height;y++) for(x=0;x<width;x++)
  {
        *(newImg->bitmap +x+y*width) = *(newImg->bitmap +x+y*width) /max;
  }

  free(tmp->bitmap);
  free(tmp);
  free(kernel);
}

/* Function: tophatRGB
   Subtracts local background from a RGB image by first convolving using a flat kernel, and then subtracting the result from the original image 

   Parameters:

     img - Pointer to image
     newImg - Pointer to image
     sigmax - kernel width in x-direction
     sigmay - kernel width in y-direction
*/
void tophatRGB(tImage_uni* img, tImage_uni* newImage, float sigmax, float sigmay)
{
  //printf("tophat! %f %f\n", sigmax, sigmay);
  int i,x,y;
  double som,t, t_r, t_g, t_b;
  int width=img->width,height=img->height;
  int radiusx = (int) ceil(sigmax*0.5);
  int radiusy = (int) ceil(sigmax*0.5);
  int diameterx=radiusx*2+1; // always odd
  int diametery=radiusy*2+1;

  tImage_uni *tmp = (tImage_uni*) malloc(sizeof(tImage_uni));
 //create an empty image
  tmp->width=width;
  tmp->height=height;
  tmp->bitmap= (float*) malloc(width*height*sizeof(float));

  newImage->width=width;
  newImage->height=height;
  newImage->bitmap= (float*) malloc(width*height*sizeof(float));

  tmp->r= (float*) malloc(width*height*sizeof(float));
  tmp->g= (float*) malloc(width*height*sizeof(float));
  tmp->b= (float*) malloc(width*height*sizeof(float));
  newImage->r= (float*) malloc(width*height*sizeof(float));
  newImage->g= (float*) malloc(width*height*sizeof(float));
  newImage->b= (float*) malloc(width*height*sizeof(float));

  //create 1d Gaussian kernel for convolution
  float* kernelx= (float*) malloc(diameterx*sizeof(float));
  float* kernely= (float*) malloc(diametery*sizeof(float));
  for(i=0;i<diameterx;i++)  kernelx[i]=1;
  for(i=0;i<diametery;i++)  kernely[i]=1;

  //printf("\n");
  //printf("Performing Tophat convolutions.. \n \n");

  //printf("Tophat 1nd step\n");

  for(y=0;y<height;y++)
  {
        for(x=0;x<width;x++)
        {
                t=0.0;
                t_r=0.0;
                t_g=0.0;
                t_b=0.0;
                som=0.0;
                for(i=0;i<diameterx;i++)
                {
                        if(x+(i-radiusx) > 0 && (x+(i-radiusx) < width))
                        {
                                t+=*(img->bitmap +x+i-radiusx + y*width)*kernelx[i];
        t_r+=*(img->r +x+i-radiusx + y*width)*kernelx[i];
        t_g+=*(img->g +x+i-radiusx + y*width)*kernelx[i];
        t_b+=*(img->b +x+i-radiusx + y*width)*kernelx[i];
                                som+=kernelx[i];
                        }
                }
                //      printf("%f\n", som);
                if(som != 0)
                {
                        *(tmp->bitmap +x+y*width)=t/som;
      *(tmp->r +x+y*width)=t_r/som;
      *(tmp->g +x+y*width)=t_g/som;
      *(tmp->b +x+y*width)=t_b/som;
                }
                else
                {
                        *(tmp->bitmap +x+y*width)=0.0;
      *(tmp->r +x+y*width)= 0.0;
      *(tmp->g +x+y*width)= 0.0;
      *(tmp->b +x+y*width)= 0.0;
                }
        }
  }

  //Do the second one 
  // printf("Tophat 2nd step\n");

  for(y=0;y<height;y++)
  {
          for(x=0;x<width;x++)
          {
                t=0.0;
                t_r=0.0;
                t_g=0.0;
                t_b=0.0;
                som=0.0;
                for(i=0;i<diametery;i++)
                {
                        if(y+(i-radiusy) > 0 && (y+(i-radiusy) < height))
                        {
                                t+=*(tmp->bitmap + x + (y+i-radiusy)*width)*kernely[i];
        t_r+=*(tmp->r + x + (y+i-radiusy)*width)*kernely[i];
        t_g+=*(tmp->g + x + (y+i-radiusy)*width)*kernely[i];
        t_b+=*(tmp->b + x + (y+i-radiusy)*width)*kernely[i];
                                som+=kernely[i];
                        }
                }

                if(som != 0)
                {
                        *(newImage->bitmap +x+y*width)=t/som;
      *(newImage->r +x+y*width)=t_r/som;
      *(newImage->g +x+y*width)=t_g/som;
      *(newImage->b +x+y*width)=t_b/som;
                }
                else
                {
                        *(newImage->bitmap +x+y*width)=0;
      *(newImage->r +x+y*width)=0;
      *(newImage->g +x+y*width)=0;
      *(newImage->b +x+y*width)=0;
                }
          }
  }

  // subtract tophat from the original (or gaussian blurred) image.
  // printf("Subtracting tophat from the original (already blurred) image.. \n");
  for(y=0;y<height;y++)
  {
          for(x=0;x<width;x++)
          {
                *(newImage->bitmap +x+y*width) = *(img->bitmap +x+y*width) - *(newImage->bitmap +x+y*width);
    *(newImage->r +x+y*width) = *(img->r +x+y*width) - *(newImage->r +x+y*width);
    *(newImage->g +x+y*width) = *(img->g +x+y*width) - *(newImage->g +x+y*width);
    *(newImage->b +x+y*width) = *(img->b +x+y*width) - *(newImage->b +x+y*width);

                if (*(newImage->bitmap +x+y*width) < 0)
                        *(newImage->bitmap +x+y*width) = 0;

    
    if (*(newImage->r +x+y*width) < 0)
      *(newImage->r +x+y*width) = 0;
    if (*(newImage->g +x+y*width) < 0)
      *(newImage->g +x+y*width) = 0;
    if (*(newImage->b +x+y*width) < 0)
      *(newImage->b +x+y*width) = 0;
          }
  }

  free(tmp->bitmap);
  free(tmp->r);
  free(tmp->g);
  free(tmp->b);

  free(tmp);
  free(kernelx);
  free(kernely);
}

/* Function: tophat
   Subtracts local background from a monochrome image by first convolving using a flat kernel, and then subtracting the result from the original image 

   Parameters:

     img - Pointer to image
     newImg - Pointer to image
     sigmax - kernel width in x-direction
     sigmay - kernel width in y-direction
*/
void tophat(tImage_uni* img, tImage_uni* newImage, float sigmax, float sigmay)
{
  int i,x,y;
  double som, t;
  int width=img->width,height=img->height;
  int radiusx = (int) ceil(sigmax*0.5);
  int radiusy = (int) ceil(sigmax*0.5);
  int diameterx=radiusx*2+1; // always odd
  int diametery=radiusy*2+1;
  newImage->width=width;
  newImage->height=height;
  newImage->bitmap= (float*) malloc(width*height*sizeof(float));

  if (sigmax > 0.0)
  {
	  //printf("tophat! %f %f\n", sigmax, sigmay);

	  tImage_uni *tmp = (tImage_uni*) malloc(sizeof(tImage_uni));
	  
	  //create an empty image
	  tmp->width=width;
	  tmp->height=height;
	  tmp->bitmap= (float*) malloc(width*height*sizeof(float));

	  //create 1d Gaussian kernel for convolution
	  float* kernelx= (float*) malloc(diameterx*sizeof(float));
	  float* kernely= (float*) malloc(diametery*sizeof(float));
	  for(i=0;i<diameterx;i++)  kernelx[i]=1;
	  for(i=0;i<diametery;i++)  kernely[i]=1;

	  //printf("\n");
	  //printf("Performing Tophat convolutions.. \n \n");

	  //printf("Tophat 1nd step\n");

	  for(y=0;y<height;y++) 
	  {
	  for(x=0;x<width;x++)
	    {
	    t=0.0;
	    som=0.0;
	    for(i=0;i<diameterx;i++)
	    {
	      if(x+(i-radiusx) > 0 && (x+(i-radiusx) < width))
	      {
		t+=*(img->bitmap +x+i-radiusx + y*width)*kernelx[i];
		som+=kernelx[i];
	      }
	    }
	    //  printf("%f\n", som);
	    if(som != 0)
	    {
	      *(tmp->bitmap +x+y*width)=t/som;
	    }
	    else
	    {
	      *(tmp->bitmap +x+y*width)=0.0;
	    }
	  }
	  }

	  //Do the second one 
	  // printf("Tophat 2nd step\n");

	  for(y=0;y<height;y++) 
	  {
	    for(x=0;x<width;x++)
	    {
	    t=0.0;
	    som=0.0;
	    for(i=0;i<diametery;i++)
	    {
	      if(y+(i-radiusy) > 0 && (y+(i-radiusy) < height))
	      {
		t+=*(tmp->bitmap + x + (y+i-radiusy)*width)*kernely[i];
		som+=kernely[i];
	      }
	    }

	    if(som != 0)
	    {
	      *(newImage->bitmap +x+y*width)=t/som;
	    }
	    else
	    {
	      *(newImage->bitmap +x+y*width)=0;
	    }
	    }
	  }
	  // subtract tophat from the original (or gaussian blurred) image.
	  // printf("Subtracting tophat from the original (already blurred) image.. \n");

	  for(y=0;y<height;y++)
	  {
		  for(x=0;x<width;x++)
		  {
			*(newImage->bitmap +x+y*width) = *(img->bitmap +x+y*width) - *(newImage->bitmap +x+y*width);

			if (*(newImage->bitmap +x+y*width) < 0)
				*(newImage->bitmap +x+y*width) = 0;
		  }
	  }

	  free(tmp->bitmap);
	  free(tmp);
	  free(kernelx);
	  free(kernely);
  }
  else
  {
	// no tophat, simply copy the image
	for(y=0;y<height;y++)
        {
        	for(x=0;x<width;x++)
                {
                        *(newImage->bitmap +x+y*width) = *(img->bitmap +x+y*width);
                }
        }
  }
}

/* Function: tophatBackground
   Subtracts local background from a background image by first convolving using a flat kernel, and then subtracting the result from the original image 

   Parameters:

     img - Pointer to image
     newImg - Pointer to image
     sigmax - kernel width in x-direction
     sigmay - kernel width in y-direction
*/
void tophatBackground(tImage_uni* img, tImage_uni* newImage, float sigmax, float sigmay)
{
  //printf("tophat! %f %f\n", sigmax, sigmay);
  int i,x,y;
  double som,t;
  int width=img->width,height=img->height;
  int radiusx = (int) ceil(sigmax*0.5);
  int radiusy = (int) ceil(sigmax*0.5);
  int diameterx=radiusx*2+1; // always odd
  int diametery=radiusy*2+1;

  tImage3d *tmp = (tImage3d*) malloc(sizeof(tImage3d));
 //create an empty image
  tmp->width=width;
  tmp->height=height;
  tmp->bitmap= (float*) malloc(width*height*sizeof(float));
  newImage->width=width;
  newImage->height=height;
  newImage->bitmap= (float*) malloc(width*height*sizeof(float));

  //create 1d Gaussian kernel for convolution
  float* kernelx= (float*) malloc(diameterx*sizeof(float));
  float* kernely= (float*) malloc(diametery*sizeof(float));
  for(i=0;i<diameterx;i++)  kernelx[i]=1;
  for(i=0;i<diametery;i++)  kernely[i]=1;

  //printf("\n");
  //printf("Performing Tophat convolutions.. \n \n");

  //printf("Tophat 1nd step\n");

  for(y=0;y<height;y++) for(x=0;x<width;x++){
    t=0;
    som=0;
    for(i=0;i<diameterx;i++){
      if(x+(i-radiusx) > 0 && (x+(i-radiusx) < width))
          {
        t+=*(img->bitmap +x+i-radiusx + y*width)*kernelx[i];
        som+=kernelx[i];
      }
    }
//  printf("%f\n", som);
    if(som != 0)
      *(tmp->bitmap +x+y*width)=t/som;
    else
      *(tmp->bitmap +x+y*width)=0.0;
  }
  //Do the second one 
  // printf("Tophat 2nd step\n");

  for(y=0;y<height;y++) for(x=0;x<width;x++){
    t=0;
    som=0;
    for(i=0;i<diametery;i++){
      if(y+(i-radiusy) > 0 && (y+(i-radiusy) < height))
          {
        t+=*(tmp->bitmap + x + (y+i-radiusy)*width)*kernely[i];
        som+=kernely[i];
      }
    }
    if(som != 0)
      *(newImage->bitmap +x+y*width)=t/som;
    else
      *(newImage->bitmap +x+y*width)=0;
  }

  // subtract tophat from the original (or gaussian blurred) image.
  // printf("Subtracting tophat from the original (already blurred) image.. \n");

  for(y=0;y<height;y++)
  {
          for(x=0;x<width;x++)
          { //!
    //printf("%f %f\n", *(img->bitmap +x+y*width), *(newImage->bitmap +x+y*width)); 
                *(newImage->bitmap +x+y*width) = *(img->bitmap +x+y*width) - *(newImage->bitmap +x+y*width);
                //*(newImage->bitmap +x+y*width) = *(img->bitmap +x+y*width);
    //printf("%f ", *(img->bitmap +x+y*width))
                if (*(newImage->bitmap +x+y*width) < 0)
                        *(newImage->bitmap +x+y*width) = 0;
          }
  }

  free(tmp->bitmap);
  free(tmp);
  free(kernelx);
  free(kernely);
}

/* Function: tophatRGB
   Subtracts a value from each pixel in a monochrome image

   Parameters:

     a - value to be subtracted
     img - Pointer to image
*/
void subBackground(double a, tImage_uni *img) 
{
  unsigned int i;
  for(i=0;i<img->width*img->height;i++) 
  {
    *(img->bitmap+i)-=a;
    if(*(img->bitmap+i) <0) *(img->bitmap+i)=0;
  }
}

/* Function: intialiseHistogram
   Intialises values of histogram

   Parameters:

     histogram - histogram for single channel
     histogram_r - histogram for red channel
     histogram_g - histogram for green channel
     histogram_b - histogram for blue channel
     values - values of the pixels corresponding to the bins in the histogram(s)
     BINS - number of bins+1 in the histogram
*/
void initialiaseHistogram(int* histogram, int* histogram_r, int* histogram_g, int* histogram_b, double* values, int BINS)
{
   double MAX = 1.0;
   int n=0;
   for (n = 0; n <= BINS; n++)
   {
         histogram[n] = 0;
         histogram_r[n] = 0;
         histogram_g[n] = 0;
         histogram_b[n] = 0;
         values[n] = ((double)n/(double)BINS)* MAX;
   }
}

/* Function: process
   Passes various filters over the input images

   Parameters:

     start - end frame in the sub-dataset
     stop - end frame in the sub-dataset
     images - point to a list of images
     images_bg - point to a composite background image
     tophatdiameter - kernel diameter used in the tophat function
     blurdiameter - kernel diameter used in blurring functions
     bacteriaAreWhite - set to 1 if bacteria are areas with high intensities (white), or 0 if low intensities (black)
     RGB - set to 1 if RGB images, 0 for monochrome
     outputdir - output directory
     DEBUG - set to 0 for minimal output, 1 or higher to print extra DEBUG information
*/
void process(int start, int stop, tImage_uni* images, tImage_uni* images_bg, int tophatdiameter, double blurdiameter, int bacteriaAreWhite, int RGB, char* outputdir, int DEBUG) 
{
  // images_bg

  int i, x,y;
  int width, height;
  //tImage *tmp = (tImage*) malloc(sizeof(tImage));

  tImage_uni *img=images;

  width=img->width; // assuming they are the same for all images in the series of images
  height=img->height;

  if (DEBUG)
  	printf("Image dimensions (width, height): %i %i \n", width, height);

  int BINS = 5000;
  int MAX = 1;

  int* histogram_original = (int*) malloc((BINS+1)*sizeof(int));
  int* histogram_original_r = (int*) malloc((BINS+1)*sizeof(int));
  int* histogram_original_g = (int*) malloc((BINS+1)*sizeof(int));
  int* histogram_original_b = (int*) malloc((BINS+1)*sizeof(int));

  int* histogram_filtered = (int*) malloc((BINS+1)*sizeof(int));
  int* histogram_filtered_r = (int*) malloc((BINS+1)*sizeof(int));
  int* histogram_filtered_g = (int*) malloc((BINS+1)*sizeof(int));
  int* histogram_filtered_b = (int*) malloc((BINS+1)*sizeof(int));

  int* histogram_final = (int*) malloc((BINS+1)*sizeof(int));
  int* histogram_final_r = (int*) malloc((BINS+1)*sizeof(int));
  int* histogram_final_g = (int*) malloc((BINS+1)*sizeof(int));
  int* histogram_final_b = (int*) malloc((BINS+1)*sizeof(int));

  double* pct = (double*) malloc((BINS+1)*sizeof(double));
  double* pct_r = (double*) malloc((BINS+1)*sizeof(double));
  double* pct_g = (double*) malloc((BINS+1)*sizeof(double));
  double* pct_b = (double*) malloc((BINS+1)*sizeof(double));
  double* values = (double*) malloc((BINS+1)*sizeof(double));

  initialiaseHistogram(histogram_original, histogram_original_r, histogram_original_g, histogram_original_b, values, BINS);
  initialiaseHistogram(histogram_filtered, histogram_filtered_r, histogram_filtered_g, histogram_filtered_b, values, BINS);
  initialiaseHistogram(histogram_final, histogram_final_r, histogram_final_g, histogram_final_b, values, BINS);

  int PIXELS =0;
  float max = 0.0;
  float max_r = 0.0;
  float max_g = 0.0;
  float max_b = 0.0;

  if (DEBUG)
  {
	  if (RGB==1)
	    printf("Loading 3-channel RGB images.\n");
	  else
	    printf("Loading monochrome images.\n");
  }

  for (i=start; i < stop; i++) {
    if (DEBUG>1)
      printf("Preprocessing frame: %i - %i %i.\n",i, start, stop);
    //printf("%i\n", i);
    //total++;
    img = images+i;

    if (RGB)
      printf("No need to reserve memory for colour images here, should have been already done. \n");

    for(y=0;y<height;y++) {
      for(x=0;x<width;x++) {
        PIXELS++;

        if (RGB) 
        {
          *(img->r +x+y*width) = 1.0-*(img->r +x+y*width);
	  if (*(img->r +x+y*width) < 0)
		*(img->r +x+y*width) = 0;
          *(img->g +x+y*width) = *(img->g +x+y*width);
	  if (*(img->b +x+y*width) < 0)
		*(img->b +x+y*width) = 0;
          *(img->b +x+y*width) = 1.0-*(img->b +x+y*width);
	  if (*(img->b +x+y*width) < 0)
		*(img->b +x+y*width) = 0;
          *(img->bitmap +x+y*width) = *(img->g +x+y*width); // filling bitmap with green channel
          //printf("*--> %f %f %f\n", *(img->r +x+y*width), *(img->g +x+y*width), *(img->b +x+y*width) );
        }

        if (bacteriaAreWhite) 
        {
          *(img->bitmap +x+y*width) = *(img->bitmap +x+y*width); 
        } 
	else 
        {
          double pixelvalue = 1.0-*(img->bitmap +x+y*width);
          *(img->bitmap +x+y*width) = pixelvalue; // invert black and white
	  if (pixelvalue < 0)
	  {
		if (DEBUG >= 2)
			printf("negative pixel value detected (%f).. settting to zero\n", pixelvalue);	
		*(img->bitmap +x+y*width) = 0.0;
	  }	
        }

        ///////////////////////////////////

        if (*(img->bitmap +x+y*width) > max) max = *(img->bitmap +x+y*width);

        int bin = (int)(( *(img->bitmap +x+y*width) / MAX ) * BINS);
	if (bin < 0)
	{
		printf("negative pixel value detected.. should have been fixed.. exiting!\n");	
		exit(666);
	}
        histogram_original[bin]++;

        if (RGB) {
          if (*(img->r +x+y*width) > max_r)   max_r = *(img->r +x+y*width);

          if (*(img->g +x+y*width) > max_g)   max_g = *(img->g +x+y*width);

          if (*(img->b +x+y*width) > max_b)   max_b = *(img->b +x+y*width);

          int bin_r = (int)(( *(img->r +x+y*width) / MAX ) * BINS);
          histogram_original_r[bin_r]++;

          int bin_g = (int)(( *(img->g +x+y*width) / MAX ) * BINS);
          histogram_original_g[bin_g]++;

          int bin_b = (int)(( *(img->b +x+y*width) / MAX ) * BINS);
          histogram_original_b[bin_b]++;
        }
      }
    }

    tImage_uni fil;
    fil.width=img->width;
    fil.height=img->height;
    fil.bitmap= (float*) malloc(fil.width*fil.height*sizeof(float));

    if (DEBUG >= 5)
      printf("Blurring.\n");

    //if(blurdiameter > 0)
    //{      
      // Simple Gaussian blur
      if (!RGB)
        blur(img, &fil,blurdiameter);
      else
        blurRGB(img, &fil,blurdiameter);
    //}

    tImage_uni newImg;

    if (DEBUG >= 5)
      printf("Tophat.\n");

    if (!RGB)
      tophat(&fil, &newImg, tophatdiameter, tophatdiameter);
    else if (RGB)
      tophatRGB(&fil, &newImg, tophatdiameter, tophatdiameter);

    tImage_uni* new_p = &newImg;

    ///////////////////////////////////////

    if (DEBUG >= 5)
      printf("Leveling image..\n");

    for(y=0;y<height;y++) {
      for(x=0;x<width;x++) {
        if(images_bg !=NULL)
          // subtract background signals such as signal from dirt in the optical pathway
          *(new_p->bitmap +x+y*width) = *(new_p->bitmap +x+y*width)-*(images_bg->bitmap+x+y*width);

        // ensure that the image doesn't have negative values:
        if (*(new_p->bitmap +x+y*width) < 0.0)
          *(new_p->bitmap +x+y*width) = 0.0;
      }
    }

    for(y=0;y<height;y++) {
      for(x=0;x<width;x++) {
        if (*(new_p->bitmap +x+y*width) > max) // if value of this pixel is higher than current maximum
          max = *(new_p->bitmap +x+y*width); // maximum intensity found

        int bin = (int)(( *(new_p->bitmap +x+y*width) / MAX ) * BINS); // index of the bin
        histogram_filtered[bin]++; // increase value in bin

        if (RGB) {
          // in case the image is an rgb image, all three channels have to be evaluated
          if (*(new_p->r +x+y*width) > max_r)
            max_r = *(new_p->r +x+y*width);

          if (*(new_p->g +x+y*width) > max_g)
            max_g = *(new_p->g +x+y*width);

          if (*(new_p->b +x+y*width) > max_b)
            max_b = *(new_p->b +x+y*width);

          int bin_r = (int)(( *(new_p->r +x+y*width) / MAX ) * BINS);
          histogram_filtered_r[bin_r]++;

          int bin_g = (int)(( *(new_p->g +x+y*width) / MAX ) * BINS);
          histogram_filtered_g[bin_g]++;

          int bin_b = (int)(( *(new_p->b +x+y*width) / MAX ) * BINS);
          histogram_filtered_b[bin_b]++;
        }
      }
    }

    if (DEBUG >= 10)
      printf("Copying image.. \n");

    for(y=0;y<height;y++) 
    {
      for(x=0;x<width;x++) 
      {
        // copying pixel value
        *(img->bitmap +x+y*width) = *(newImg.bitmap +x+y*width);
        if (RGB)
        {
          // copying pixel value for all three channels
          *(img->r +x+y*width) = *(newImg.r +x+y*width);
          *(img->g +x+y*width) = *(newImg.g +x+y*width);
          *(img->b +x+y*width) = *(newImg.b +x+y*width);
        }
      }
    }

    if (DEBUG >= 2)
      printf("Freeing memory from filtering steps..\n");

    free(newImg.bitmap);
    free(fil.bitmap);

    if (RGB)
    {
      free(newImg.r);
      free(fil.r);
      free(newImg.g);
      free(fil.g);
      free(newImg.b);
      free(fil.b);
    }

  }
  // end iteration over all the image

  if (DEBUG >= 5)
    printf("Pixels processed: %i\n", PIXELS);

  int n = 0;
  int tot = 0;
  for (n = 0; n < BINS; n++)
  {
    tot += histogram_original[n];
    pct[n] = histogram_original[n]/(double)PIXELS;
    pct_r[n] = histogram_original_r[n]/(double)PIXELS;
    pct_g[n] = histogram_original_g[n]/(double)PIXELS;
    pct_b[n] = histogram_original_b[n]/(double)PIXELS;
  }

  if (DEBUG >= 5)
    printf("Pixels evaluated: %i\n", PIXELS);

  char buffer[1024];
  snprintf(buffer, sizeof(buffer), "%shist_original.dat", outputdir);
  FILE* fp_hist = fopen(buffer, "w");

  for (n = 0; n < BINS; n++)
  {
    fprintf(fp_hist, "%f %f %f %f %f %i %i %i %i\n", values[n], pct[n], pct_r[n], pct_g[n], pct_b[n], histogram_original[n], histogram_original_r[n], histogram_original_g[n], histogram_original_b[n]);
  }
  fclose(fp_hist);

  if (DEBUG >= 5)
    printf("Done initial processing images..\n");

  if (DEBUG >= 5)
    printf("Making histogram after blurring and tophat.. \n");

  // writing histogram to an output file

  tot = 0;
  for (n = 0; n < BINS; n++)
  {
    tot += histogram_filtered[n];
    pct[n] = histogram_filtered[n]/(double)PIXELS;
    pct_r[n] = histogram_filtered_r[n]/(double)PIXELS;
    pct_g[n] = histogram_filtered_g[n]/(double)PIXELS;
    pct_b[n] = histogram_filtered_b[n]/(double)PIXELS;
  }

  if (DEBUG >= 5)
    printf("Pixels evaluated: %i\n", PIXELS);

  snprintf(buffer, sizeof(buffer), "%shist_after_filtering.dat", outputdir);
  fp_hist = fopen(buffer, "w");
  for (n = 0; n < BINS; n++)
  {
    fprintf(fp_hist, "%f %f %f %f %f %i %i %i %i\n", values[n], pct[n], pct_r[n], pct_g[n], pct_b[n], histogram_filtered[n], histogram_filtered_r[n], histogram_filtered_g[n], histogram_filtered_b[n]);
  }
  fclose(fp_hist);

  // Ideally, we would like to normalise the image to its maximum value. However, there is often a small fraction of pixels that is 
  // extremely bright (can have multiple reasons). For most data sets, this is about 1 in 10 million pixels (note that one 1000x1000 pixel 
  // image already contains 1 million pixels. To deal with this, we make a histogram of intensities, and eliminate the outliers.
  double border2 = 0.9999995;

  if (RGB)
    border2 = 0.99;

  double upper_bound = 1.0;
  double upper_bound_r = 1.0;
  double upper_bound_g = 1.0;
  double upper_bound_b = 1.0;

  double score = 0.0;
  double score_r = 0.0;
  double score_g = 0.0;
  double score_b = 0.0;
  int m = 0;

  for (n = 0; n < BINS && score < border2; n++)
  {
    score+=pct[n];
    m = n;
  }

  upper_bound = values[m]; // the upper_bound corresponds to the value where to cut-off the distribution and set the maximum to use for normalisation.

  if(DEBUG >= 2) 
    printf("Upper bound: %f (> %.12f pct) \n", upper_bound, score*100);

  if (RGB)
  {
    m=0;
    for (n = 0; n < BINS && score_r < border2; n++)
    {
      score_r+=pct_r[n];
      m = n;
    }
    upper_bound_r = values[m];

    m=0;
    for (n = 0; n < BINS && score_g < border2; n++)
    {
      score_g+=pct_g[n];
      m = n;
    }
    upper_bound_g = values[m];

    m=0;
    for (n = 0; n < BINS && score_b < border2; n++)
    {
      score_b+=pct_b[n];
      m = n;
    }
    upper_bound_b = values[m];

    printf("Red upper bound: %f (> %f pct) \n", upper_bound_r, score_r*100);
    printf("Green upper bound: %f (> %f pct) \n", upper_bound_g, score_g*100);
    printf("Blue upper bound: %f (> %f pct) \n", upper_bound_b, score_b*100);
  }

  if(DEBUG >= 2)
    printf("Scaling.. \n");

  double inv_bound = 1.0/upper_bound;
  double inv_bound_r = 1.0/255.0;
  double inv_bound_g = 1.0/255.0;
  double inv_bound_b = 1.0/255.0;
  
  if(DEBUG >= 2)
    printf("upper_bound=%f, inv_bound=%f.. \n", upper_bound, inv_bound);

  if (RGB)
  {
    inv_bound_r = 1.0/upper_bound_r;
    inv_bound_g = 1.0/upper_bound_g;
    inv_bound_b = 1.0/upper_bound_b;
  }

  PIXELS = 0;
  for (i=start; i < stop; i++)
  {
    img = images+i;

    for(y=0;y<height;y++)
    {
      for(x=0;x<width;x++)
      {
        if ( *(img->bitmap +x+y*width) <= upper_bound )
          *(img->bitmap +x+y*width) = *(img->bitmap +x+y*width)*inv_bound;
        else
          *(img->bitmap +x+y*width) = 1.0;

        PIXELS++;

        int bin = (int)(( *(img->bitmap +x+y*width)  ) * BINS);
        histogram_final[bin]++;

        if (RGB)
        {
          if ( *(img->r +x+y*width) <= upper_bound_r )
            *(img->r +x+y*width) = *(img->r +x+y*width)*inv_bound_r;
          else
            *(img->r +x+y*width) = 1.0;

          if ( *(img->g +x+y*width) <= upper_bound_g )
            *(img->g +x+y*width) = *(img->g +x+y*width)*inv_bound_g;
          else
            *(img->g +x+y*width) = 1.0;

          if ( *(img->b +x+y*width) <= upper_bound_b )
            *(img->b +x+y*width) = *(img->b +x+y*width)*inv_bound_b;
          else
            *(img->b +x+y*width) = 1.0;

          int bin_r = (int)(( *(img->r +x+y*width)  ) * BINS);
          histogram_final_r[bin_r]++;

          int bin_g = (int)(( *(img->g +x+y*width) ) * BINS);
          histogram_final_g[bin_g]++;

          int bin_b = (int)(( *(img->b +x+y*width) ) * BINS);
          histogram_final_b[bin_b]++;
        }
      }
    }
  }
  if(DEBUG >= 5)
    printf("Pixels processed: %i\n", PIXELS);

  if(DEBUG >= 2)
    printf("done.. \n");

  if (RGB)
  {
    double onethird = (1.0/3.0); // Factor to make each channel weigh 1/3. Defined here for optimization, because divisions are expensive. Arguably, one could also leave this factor away since the images get normalised later on anyway..
    double max = 0.0;
    for (i=start; i < stop; i++)
    {
      img = images+i;
      for(y=0;y<height;y++)
      {
        for(x=0;x<width;x++)
        {
          *(img->bitmap +x+y*width) = onethird*(*(img->r+x+y*width))+onethird*(*(img->g+x+y*width))+onethird*(*(img->b+x+y*width));
        }
      }
    }

    for (i=start; i < stop; i++)
    {
      img = images+i;
      for(y=0;y<height;y++)
      {
        for(x=0;x<width;x++)
        {
          if (*(img->bitmap +x+y*width) > max)
            max = *(img->bitmap +x+y*width);
        }
      }
    }

    // normalisation
    for (i=start; i < stop; i++)
    {
      img = images+i;
      for(y=0;y<height;y++)
      {
        for(x=0;x<width;x++)
        {
          *(img->bitmap +x+y*width) = *(img->bitmap +x+y*width) / max;
        }
      }
    }
  }

  tot = 0;
  for (n = 0; n < BINS; n++)
  {
    pct[n] = histogram_final[n]/(double)PIXELS;
    tot += histogram_final[n];

    if (RGB)
    {
      pct_r[n] = histogram_final_r[n]/(double)PIXELS;
      pct_g[n] = histogram_final_g[n]/(double)PIXELS;
      pct_b[n] = histogram_final_b[n]/(double)PIXELS;
    } 
  }
  if (DEBUG >= 5)
    printf("Pixels evaluated: %i\n", PIXELS);

  snprintf(buffer, sizeof(buffer), "%shist_final.dat", outputdir);
  fp_hist = fopen(buffer, "w");

  for (n = 0; n < BINS; n++)
  {
    fprintf(fp_hist, "%f %f %f %f %f %i %i %i %i\n", values[n], pct[n], pct_r[n], pct_g[n], pct_b[n], histogram_final[n], histogram_final_r[n], histogram_final_g[n], histogram_final_b[n]);
  }
  fclose(fp_hist);


  if(DEBUG >= 2)
    printf("Freeing memory.. \n");

  free(histogram_original);
  free(histogram_original_r);
  free(histogram_original_g);
  free(histogram_original_b);

  free(histogram_filtered);
  free(histogram_filtered_r);
  free(histogram_filtered_g);
  free(histogram_filtered_b);

  free(histogram_final);
  free(histogram_final_r);
  free(histogram_final_g);
  free(histogram_final_b);

  free(pct);
  free(pct_r);
  free(pct_g);
  free(pct_b);

  free(values);
  if(DEBUG >= 2)
    printf("Done.. \n");
}
