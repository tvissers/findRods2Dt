------------------------------------------------------------------------------
About: License

    This file is part of findRods2Dt.

    findRods2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    findRods2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with findRods2Dt.  If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
About: This program

findRods2Dt is designed to find E. coli bacteria in 2D phase-contrast microscopy images.
 
This code builds upon earlier software designed and written by Michiel Hermes to find the positions and orientations of rod-shaped colloids, for which the algorithm and its applications are described in detail elsewhere (Besseling, T. H., et al. Journal of Physics: Condensed Matter 27.19 (2015): 194109). 

Teun Vissers and Michiel Hermes altered and optimised existing code and designed and added new parts to make it suitable for 2D data-sets of microscopy images containing rod-shaped bacteria. 

Please cite the following papers if you use this code for a scientific publication:

T. Vissers et al., Science Advances, 4, 4, eaao1170, 2018.
https://doi.org/10.1126/sciadv.aao1170

T. Vissers et al., PloS One., 14, 6, e0217823, 2019.
https://doi.org/10.1371/journal.pone.0217823

--------------------------------------------------------------------------------

About: repository

At the time of publication, a repository for this code is available at: https://git.ecdf.ed.ac.uk/tvissers/findRods2Dt
Updated versions may appear there.

--------------------------------------------------------------------------------
About: Dependencies

This work requires libtiff and gsl to be installed. In Linux Ubuntu these can be installed by typing:
> sudo apt install libgsl-dev libtiff5-dev

A makefile is provided. Typing:
> make
should be sufficient. 

--------------------------------------------------------------------------------
About: Running the example

Type 'make' to compile the code and create the executable.

To run the testcase a findRods2Dt.in should be provided with this program. This file
contains the setting used by the findRods2Dt algorithm and should look as follows:

--- text -----------------------------------------------------------------------
FILEMASK ../testcase/input/image000*.tif
FILEMASKBG ../testcase/background/*.tif
POSITIONSFILE ../testcase/output/positions.dat
OUTPUTDIR ../testcase/output/
FINDRODS 1
MINVAL 0.25
DIAMETER 2.8
BLURDIAMETER 0.8
SUBBACKGROUND 0.0000001
BGTHRESHOLD 0.40
DEBUG 1
OVERLAPR 1.0
TOPHATDIAMETER 15
MININTENS 5
WRITEFREQ 1
TRACERR 2
WHITE 0
RGB 0
--------------------------------------------------------------------------------
where FILEMASK and FILEMASKBG should point to the directories containing the
example data set. 
For a detailed description of these parameters see findRods2Dt.c. If all images are in 
place the example can be run by:
> ./findRods2Dt 
This will produce output data in testcase/output.
Make sure the output directory exists before running the code.

A bash script making the input file, and running the program can be executed by:
>./run.sh

Files: Code files 
findRods2Dt.c - The tracking algorithm.
filter.c - Routines to process and filter images. 
tiffio.c - Handles the reading and writing of tiff files.
namelist.c - Reads in the global variables from findRods2Dt.in.

Files: Example data

testcase/input/ - Image tiff files of E. coli.  
testcase/background/ - Background tiff files.

--------------------------------------------------------------------------------

About: Input images

* Input images should be in tiff format. File names should contain numbers to label consecutive images in a time series.

--------------------------------------------------------------------------------

About: Output files

* positions.dat file containing positions and orientations of rod-shaped objects.
* frames used to determine positions and orientations (after filtering).
* frames depicting identified rod-shaped objects.
* background images used to subtract false positives.
* histograms of pixel intensities at various steps of the analysis.


--------------------------------------------------------------------------------

About: positions.dat - file format

--- text -----------------------------------------------------------------------
&NUMBEROFFRAMES
&FRAMENO &NUMBEROFPARTICLESINTHISFRAME
FIELD_PIXELS_X FIELD_PIXELS_Y VERSION_ID
COM_X COM_Y COM_Z POLE_X POLE_Y POLE_Z FITPAR1 FITPAR2 FITPAR3 WIDTH LENGTH FITPAR4
COM_X COM_Y COM_Z POLE_X POLE_Y POLE_Z FITPAR1 FITPAR2 FITPAR3 WIDTH LENGTH FITPAR4
COM_X COM_Y COM_Z POLE_X POLE_Y POLE_Z FITPAR1 FITPAR2 FITPAR3 WIDTH LENGTH FITPAR4
..
..
..
COM_X COM_Y COM_Z POLE_X POLE_Y POLE_Z FITPAR1 FITPAR2 FITPAR3 WIDTH LENGTH FITPAR4
&FRAMENO &NUMBEROFPARTICLESINTHISFRAME
FIELD_PIXELS_X FIELD_PIXELS_Y VERSION_ID
..
..
--------------------------------------------------------------------------------

Legend:

--- text -----------------------------------------------------------------------
NUMBEROFFRAMES	:	total number of frames
--------------------------------------------------------------------------------

For each frame, the following info is given:

--- text -----------------------------------------------------------------------
FRAMENO		:	frame number, starts with 0
FIELD_PIXELS_X 	:	max pixel value in x-direction
FIELD_PIXELS_Y 	:	max pixel value in y-direction
VERSION_ID	:	Some file ID, typically set to -10
--------------------------------------------------------------------------------

For each found rod-shaped particle, the following info is given on each line:

--- text -----------------------------------------------------------------------
COM_X		:	x-coordinate COM
COM_Y		:	y-coordinate COM
COM_Z		:	z-coordinate (0 for 2D data set)
POLE_X		:       x-coordinate of one of the poles
POLE_Y		:	y-coordinate of one of the poles
POLE_Z		:	z-coordinate of one of the poles (0 for 2D data set)
FITPAR1		:	aspect ratio lengthfromfit/widthfromfit
FITPAR2		:	(lengthfromfit-widhtfromfit)/widthfromfit
FITPAR3		:	aspect ratio lengthfromfit / estimated width from input file
WIDTH		:	width of the rod from fit
LENGTH		:	length of the rod from fit
FITPAR4		:	sqrt(largesteigenvalue/smallesteigenvalue)
--------------------------------------------------------------------------------
